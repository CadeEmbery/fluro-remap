HomeController.resolve = {
    contacts: function(FluroContent) {
        return FluroContent.resource('contact').query({
            fields: ['title', 'data', 'realms', 'tags']
        }).$promise;
    },
    teams: function(FluroContent) {
        return FluroContent.resource('team').query({
            fields: ['title', 'definition'],
            allDefinitions: true,
        }).$promise;
    },
    tags: function(FluroContent) {
        return FluroContent.resource('tag').query({
            fields: ['title', '_id'],
            // allDefinitions: true,
        }).$promise;
    },
    definitions: function(FluroContent) {
        return FluroContent.endpoint('defined/types/contactdetail').query().$promise;
    },
    seo: function(FluroSEOService, Asset, $rootScope) {
        FluroSEOService.pageTitle = null;
        return true;
    },
}

/////////////////////////////////////////////////////

HomeController.data = {
    // requireLogin: true,
}

/////////////////////////////////////////////////////

app.controller('HomeController', HomeController);

/////////////////////////////////////////////////////

function HomeController($scope, contacts, teams, FluroContent, PathfinderService, definitions, tags) {

    $scope.state = 'ready';
    var existingTeams = teams;
    var existingTags = tags;

    ////////////////////////////////////////////////////////////

    function getFlattenedFields(array, trail) {
        return _.chain(array).map(function(field, key) {
            if (field.type == 'group') {

                var finalResult;

                //Check if we need to add the key
                var keyedGroup = field.asObject;

                //////////////////////////////////////
            
                if (keyedGroup) {
                    trail.push(field.key);
                }

                //Get the sub fields and go back up a level
                var fields = getFlattenedFields(field.fields, trail);

                if (keyedGroup) {
                    trail.pop();
                }
                //Final result
                finalResult = fields;
      
                return finalResult;

            } else {
                //We need to clone the field otherwise the keys get all whacky
                var newField = field;

                //Take a copy of the trail for this field
                newField.trail = trail.slice();

                //Add the field key to the end of the trail
                newField.trail.push(field.key);

                return newField;
            }
        })
        .flattenDeep()
        .value();

    }

    $scope.definitions = _.map(definitions, function(definition) {
        var flattenedFields = getFlattenedFields(definition.fields, []);
        definition.fields = flattenedFields;
        return definition;
    })

    ////////////////////////////////////////////

    $scope.contacts = _.filter(contacts, function(contact) {
        if (!contact.data) {
            return;
        }

        if (!_.keys(contact.data).length) {
            return;
        }

        return true;
    });

    ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////

    $scope.options = _.chain($scope.contacts)
        .map('data')
        .reduce(function(set, dataObject) {
            var flatFields = PathfinderService.extract(dataObject);

            _.each(flatFields, function(field) {
                var uniqueKey = _.kebabCase(field.key);

                var existing = set[uniqueKey];
                if(!existing) {
                    existing =
                    set[uniqueKey] = {
                        from:field.key,
                        values:[]
                    }
                } 

               existing.values.push(field.value)
            })

            return set;
        }, {})
        .values()
        .map(function(entry) {
            return {
                from:entry.from,
                exampleString:_.uniqBy(entry.values, function(v) {
                    return v;
                }).slice(0, 3).join(', ')
            }
        })
        .orderBy(function(entry) {
            return entry.from;
        })
        .value();

    console.log('OPTIONS', $scope.contacts.length, $scope.options);

    ////////////////////////////////////////////

    $scope.start = function() {
        $scope.state = 'processing';

        ///////////////////////////////////////////////////

        var activeMaps = _.chain($scope.options)

            // Make sure that if a data key option has a something selected to remap to, that the team/tag/definition is selected for that option.
            .filter(function(entry) {
                switch (entry.to) {
                    case 'definition':
                        if (!entry.definition) {
                            return;
                        }
                        break;
                    case 'team':
                        if (!entry.team || !entry.team.length) {
                            return;
                        }
                        break;
                    case 'tag':
                        if (!entry.tag || !entry.tag.length) {
                            return;
                        }
                        break;
                    default:
                        return;
                        break;
                }

                return true;

            })
            .reduce(function(set, row) {

                // check each data object key...
                switch (row.to) {
                    case 'definition':
                        var definitionName = row.definition.definitionName;

                        var existing = set.definitions[definitionName];
                        if (!existing) {
                            existing =
                                set.definitions[definitionName] = []
                        }

                        existing.push({
                            from: row.from,
                            to: _.get(row, 'field.trail').join('.'),
                            type: _.get(row, 'field.type')
                        });
                        break;
                    case 'team':
                        set.teams[row.from] = row.team;
                        break;
                    case 'tag':
                        set.tags[row.from] = row.tag;
                        break;
                }

                return set;
            }, {
                definitions: {},
                teams: {},
                tags: {}
            })
            .value();

        console.log('ACTIVE MAPS', activeMaps);

        ////////////////////////////////////////////

        $scope.progress = {
            total: $scope.contacts.length,
            current: 0,
            sheet: 0,
            team: 0,
            tagAdded: 0
        }


        // Loop through all the contacts and remap data for each contact
        async.eachSeries($scope.contacts, function(contact, next) {

            $scope.progress.current++;

            ////////////////////////////////////////////////////

            //Find all the teams this contact should be added to
            var teams = _.chain(activeMaps.teams)
                .map(function(teamTarget, teamKey) {

                    var value = _.get(contact, 'data.' + teamKey);

                    switch (teamTarget) {
                        case 'value':
                            // console.log('TEAM VALUE', value);
                            return value;
                            break;
                        case 'key':

                            var string = String(value).toLowerCase();
                            switch (string) {
                                case 'y':
                                case '1':
                                case 'yes':
                                case 'owner':
                                case 'true':
                                    return teamKey;
                                    break;
                                default:
                                    return;
                                    break;
                            }
                            break;
                    }
                })
                .flattenDeep()
                .compact()
                .value();

            ////////////////////////////////////////////////////

            //Find all the tags this contact should be added to
            var tags = _.chain(activeMaps.tags)
                .map(function(tagTarget, tagKey) {

                    // Check the current contact, and on the contact.data ojbect, get the information at the specified key.
                    var value = _.get(contact, 'data[' + tagKey + ']');

                    if(!value) {
                        return;
                    }
                    
                    ///////////////////////////////////////////////

                    

                    // tagTarget is whether to use the 'key' as the Tag, or the 'value' as the Tag.
                    switch (tagTarget) {
                        case 'value':

                            var splitOneArray = value.split(';')
                            var splitTwoArray = value.split('\n')
                            var splitThreeArray = value.split(',')

                            var tags = _.chain([splitOneArray, splitTwoArray])
                                .flatten()
                                .map(function(tagName) {
                                    return tagName.trim();
                                })
                                .compact()
                                .uniq()
                                .map(function(tagName) {
                                    return tagKey + ' - ' + tagName;
                                })
                                .value(); 

                            ///////////////////////////////////////////////

                            return tags;
                            break;
                        case 'key':
                            var string = String(value).toLowerCase();
                            switch (string) {
                                case 'y':
                                case '1':
                                case 'yes':
                                case 'true':
                                    return tagKey;
                                    break;
                                default:
                                    return;
                                    break;
                            }
                            break;
                    }
                })
                .flattenDeep()
                .compact()
                .value();

            ////////////////////////////////////////////////////

            //Find out all the detail sheets we need to create for this contact
            var detailSheets = _.chain(activeMaps.definitions)
                .map(function(entry, definitionName) {

                    var object = {
                        _external: contact._id + '-' + definitionName,
                        contact: contact._id,
                        title: contact.title + ' ' + definitionName,
                        _type: 'contactdetail',
                        definition: definitionName,
                        data: {},
                        realms: contact.realms,
                    }

                    //Loop through each field
                    var plucked = _.chain(entry)
                        .filter(function(field) {
                            var pluckedValue = _.get(contact, 'data.' + field.from);

                            if (!pluckedValue) {
                                return;
                            } else {
                                return true;
                            }
                        })
                        .map(function(field) {

                            var pluckedValue = _.get(contact, 'data.' + field.from);

                            var toField = field.to;

                            if (!toField || !toField.length) {
                                toField = _.camelCase(field.from);
                            }

                            var toPath = 'data.' + toField;

                            ///////////////////////////////////////////

                            switch (field.type) {
                                case 'boolean':

                                    var asString = String(pluckedValue).toLowerCase();
                                    //If it's a boolean
                                    switch (asString) {
                                        case 'true':
                                        case 'y':
                                        case 'yes':
                                            pluckedValue = true;
                                            break;
                                        case 'false':
                                        case 'n':
                                        case 'no':
                                            pluckedValue = false;
                                            break;
                                        default:
                                            pluckedValue = true;
                                    }
                                    break;
                            }

                            ///////////////////////////////////////////

                            console.log('Save object for ' + contact.title, toPath, pluckedValue);
                            _.set(object, toPath, pluckedValue);
                        })
                        .value();

                    if (!plucked.length) {
                        return;
                    }

                    /////////////////////////

                    return object;

                })
                .compact()
                .flattenDeep()
                .compact()
                .value();

            ///////////////////////////////////////

            var hasDetailSheets = detailSheets.length;
            var hasTeamsToJoin = teams.length;
            var hasTagsToAdd = tags.length;

            ///////////////////////////////////////

            if (!hasDetailSheets && !hasTeamsToJoin && !hasTagsToAdd) {
                return next() //Skip
            }

            ///////////////////////////////////////

            function createSheets(callback) {

                async.eachSeries(detailSheets, function(detailSheet, next) {

                    //Import the sheet
                    var request = FluroContent.endpoint('content/_import?merge=true')
                        .save(detailSheet)
                        .$promise;

                    //Request
                    request.then(function(res) {

                        $scope.progress.sheet++;
                        return next(null, res);
                    }, next);

                }, function(err) {
                    if (err) {
                        console.log(contact.title + ' sheets error', err);
                        return callback(err);
                    }

                    return callback();
                });
            }

            ///////////////////////////////////////

            function joinTeams(callback) {

                async.eachSeries(teams, function(team, next) {

                    ////////////////////////////////////////////////////////

                    var teamName = _.camelCase(team);

                    var teamObject = {
                        title: team,
                        _type: 'team',
                        realms: contact.realms,
                        _external: teamName,
                        allowProvisional: true,
                    }

                    //Import the sheet
                    var request = FluroContent.endpoint('content/_import')
                        .save(teamObject)
                        .$promise;

                    //Request
                    request.then(teamCreated, next);


                    function teamCreated(res) {

                        var teamID = res._id;

                        console.log('Join Contact', contact.title, 'to team', teamID);

                        //Now Join the contact into the team
                        var request = FluroContent.endpoint('teams/' + teamID + '/join')
                            .save({
                                _id: contact._id
                            })
                            .$promise;

                        $scope.progress.team++;

                        return next(null, res);
                    }

                    ////////////////////////////////////////////////////////

                }, function(err) {
                    if (err) {
                        console.log(contact.title + ' teams error', err);
                        return callback(err);
                    }
                    return callback();
                });
            }

            ///////////////////////////////////////

            function addTags(callback) {

                if(!contact.tags) {
                    contact.tags = [];
                }
                //Add the new tags to the contact
                var newTags = _.chain(contact.tags.concat(tags))
                .compact()
                .uniqBy(function(t) {
                    return t;
                })
                .value();

                console.log('Updating ' + contact.title + ' with ' + newTags)

                //Now Join the contact into the tag
                var request = FluroContent.resource('contact/' + contact._id)
                    .update({
                        tags: newTags,
                    })
                    .$promise
                    .then(function(res) {
                        console.log('**** Tags were: ' + res)
                        $scope.progress.tagAdded++;
                        return callback();
                    }, callback);
            }

            ///////////////////////////////////////

            //Add a second gap between the events
            setTimeout(function() {

                async.series([
                    createSheets,
                    joinTeams,
                    addTags
                ], next);

            }, 100)

        }, complete);
    }

    ////////////////////////////////////////////

    function complete(err, results) {
        if (err) {
            console.log('ERROR', err);
        } else {
            console.log('Done', results);
        }
    }
}