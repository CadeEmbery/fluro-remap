app.service('PathfinderService', function() {

    var service = {};

    /////////////////////////////

    service.extract = function(model) {

        var flat = service.flatten(model);

        /////////////////////////////

        return _.chain(flat)
            .map(function(value, key) {

                var depth = key.split('.').length;
                var indent = '';

                _.times(depth - 1, function() {
                    indent += '\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0';
                });

                var matchObject = _.get(model, key);
                var type = (typeof matchObject);

                /////////////////////////

                var readable = key + ' - ' + value;

                /////////////////////////

                if (_.isArray(matchObject)) {
                    type = 'array';
                    readable = key + ' (multiple items)';
                }
                if (_.isString(matchObject)) {
                    var momentDate = moment(matchObject, moment.ISO_8601, true);
                    var isValid = momentDate.isValid();
                    if (isValid) {
                        matchObject = momentDate.toDate();
                    }
                }
                if (_.isDate(matchObject)) {
                    type = 'date';
                }

                /////////////////////////

                return {
                    value: value,
                    key: key,
                    depth: depth,
                    type: type,
                    indent: indent,
                    readable:readable,
                };
            })
            .value();
    }

    /////////////////////////////
    /////////////////////////////
    /////////////////////////////

    service.flatten = function(obj) {
        var flattened = {}

        var circlular = []
        var circLoc = []

        function _route(prefix, value) {
            var i, len, type, keys, circularCheck, loc

            if (value == null) {
                if (prefix === "") {
                    return
                }
                flattened[prefix] = null
                return
            }
            type = typeof value
            if (typeof value == "object") {
                circularCheck = circlular.indexOf(value)
                if (circularCheck >= 0) {
                    loc = circLoc[circularCheck] || "this"
                    flattened[prefix] = "[Circular (" + loc + ")]"
                    return
                }
                circlular.push(value)
                circLoc.push(prefix)

                if (Array.isArray(value)) {
                    len = value.length

                    _route(prefix);

                    //Dont loop if the array is massive
                    if(len > 10) {
                        len = 10;
                    }

                    for (i = 0; i < len; i++) {
                        _route(prefix + "[" + i + "]", value[i])
                    }

                    return
                }

                keys = Object.keys(value)
                len = keys.length
                if (prefix) prefix = prefix + "."
                if (len == 0) _route(prefix, null)

                for (i = 0; i < len; i++) {
                    _route(prefix + keys[i], value[keys[i]])
                }
                return
            }
            flattened[prefix] = value
        }

        _route("", obj)

        return flattened
    }

    /////////////////////////////

    return service;
})