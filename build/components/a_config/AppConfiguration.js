var app = angular.module('fluro', [
    'fluro.boilerplate', // BoilerplateConfiguration.js
    // add your app specific dependencies here
]);


app.config(function($stateProvider) {

    ///////////////////////////////////////////
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    // !important not to change 'home' as the state name
    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'routes/home/home.html',
        controller: HomeController,
        data: HomeController.data,
        resolve: HomeController.resolve
    });

   
    ///////////////////////////////////////////

})
