function getMetaKey(stringKey) {
    var metas = document.getElementsByTagName("meta");
    for (i = 0; i < metas.length; i++) if (metas[i].getAttribute("property") == stringKey) return metas[i].getAttribute("content");
    return "";
}

function getFlattenedFields(array) {
    return _.chain(array).map(function(field) {
        return "group" == field.type ? (console.log("GROUP", field), getFlattenedFields(field.fields)) : field;
    }).flatten().value();
}

function _arrayBufferToBase64(buffer) {
    for (var binary = "", bytes = new Uint8Array(buffer), len = bytes.byteLength, i = 0; len > i; i++) binary += String.fromCharCode(bytes[i]);
    return window.btoa(binary);
}

function HomeController($scope, contacts, teams, FluroContent, PathfinderService, definitions, tags) {
    function getFlattenedFields(array, trail) {
        return _.chain(array).map(function(field, key) {
            if ("group" == field.type) {
                var finalResult, keyedGroup = field.asObject;
                keyedGroup && trail.push(field.key);
                var fields = getFlattenedFields(field.fields, trail);
                return keyedGroup && trail.pop(), finalResult = fields;
            }
            var newField = field;
            return newField.trail = trail.slice(), newField.trail.push(field.key), newField;
        }).flattenDeep().value();
    }
    function complete(err, results) {
        err ? console.log("ERROR", err) : console.log("Done", results);
    }
    $scope.state = "ready";
    $scope.definitions = _.map(definitions, function(definition) {
        var flattenedFields = getFlattenedFields(definition.fields, []);
        return definition.fields = flattenedFields, definition;
    }), $scope.contacts = _.filter(contacts, function(contact) {
        return contact.data && _.keys(contact.data).length ? !0 : void 0;
    }), $scope.options = _.chain($scope.contacts).map("data").reduce(function(set, dataObject) {
        var flatFields = PathfinderService.extract(dataObject);
        return _.each(flatFields, function(field) {
            var uniqueKey = _.kebabCase(field.key), existing = set[uniqueKey];
            existing || (existing = set[uniqueKey] = {
                from: field.key,
                values: []
            }), existing.values.push(field.value);
        }), set;
    }, {}).values().map(function(entry) {
        return {
            from: entry.from,
            exampleString: _.uniqBy(entry.values, function(v) {
                return v;
            }).slice(0, 3).join(", ")
        };
    }).orderBy(function(entry) {
        return entry.from;
    }).value(), console.log("OPTIONS", $scope.contacts.length, $scope.options), $scope.start = function() {
        $scope.state = "processing";
        var activeMaps = _.chain($scope.options).filter(function(entry) {
            switch (entry.to) {
              case "definition":
                if (!entry.definition) return;
                break;

              case "team":
                if (!entry.team || !entry.team.length) return;
                break;

              case "tag":
                if (!entry.tag || !entry.tag.length) return;
                break;

              default:
                return;
            }
            return !0;
        }).reduce(function(set, row) {
            switch (row.to) {
              case "definition":
                var definitionName = row.definition.definitionName, existing = set.definitions[definitionName];
                existing || (existing = set.definitions[definitionName] = []), existing.push({
                    from: row.from,
                    to: _.get(row, "field.trail").join("."),
                    type: _.get(row, "field.type")
                });
                break;

              case "team":
                set.teams[row.from] = row.team;
                break;

              case "tag":
                set.tags[row.from] = row.tag;
            }
            return set;
        }, {
            definitions: {},
            teams: {},
            tags: {}
        }).value();
        console.log("ACTIVE MAPS", activeMaps), $scope.progress = {
            total: $scope.contacts.length,
            current: 0,
            sheet: 0,
            team: 0,
            tagAdded: 0
        }, async.eachSeries($scope.contacts, function(contact, next) {
            function createSheets(callback) {
                async.eachSeries(detailSheets, function(detailSheet, next) {
                    var request = FluroContent.endpoint("content/_import?merge=true").save(detailSheet).$promise;
                    request.then(function(res) {
                        return $scope.progress.sheet++, next(null, res);
                    }, next);
                }, function(err) {
                    return err ? (console.log(contact.title + " sheets error", err), callback(err)) : callback();
                });
            }
            function joinTeams(callback) {
                async.eachSeries(teams, function(team, next) {
                    function teamCreated(res) {
                        var teamID = res._id;
                        console.log("Join Contact", contact.title, "to team", teamID);
                        FluroContent.endpoint("teams/" + teamID + "/join").save({
                            _id: contact._id
                        }).$promise;
                        return $scope.progress.team++, next(null, res);
                    }
                    var teamName = _.camelCase(team), teamObject = {
                        title: team,
                        _type: "team",
                        realms: contact.realms,
                        _external: teamName,
                        allowProvisional: !0
                    }, request = FluroContent.endpoint("content/_import").save(teamObject).$promise;
                    request.then(teamCreated, next);
                }, function(err) {
                    return err ? (console.log(contact.title + " teams error", err), callback(err)) : callback();
                });
            }
            function addTags(callback) {
                contact.tags || (contact.tags = []);
                var newTags = _.chain(contact.tags.concat(tags)).compact().uniqBy(function(t) {
                    return t;
                }).value();
                console.log("Updating " + contact.title + " with " + newTags);
                FluroContent.resource("contact/" + contact._id).update({
                    tags: newTags
                }).$promise.then(function(res) {
                    return console.log("**** Tags were: " + res), $scope.progress.tagAdded++, callback();
                }, callback);
            }
            $scope.progress.current++;
            var teams = _.chain(activeMaps.teams).map(function(teamTarget, teamKey) {
                var value = _.get(contact, "data." + teamKey);
                switch (teamTarget) {
                  case "value":
                    return value;

                  case "key":
                    var string = String(value).toLowerCase();
                    switch (string) {
                      case "y":
                      case "1":
                      case "yes":
                      case "owner":
                      case "true":
                        return teamKey;

                      default:
                        return;
                    }
                }
            }).flattenDeep().compact().value(), tags = _.chain(activeMaps.tags).map(function(tagTarget, tagKey) {
                var value = _.get(contact, "data[" + tagKey + "]");
                if (value) switch (tagTarget) {
                  case "value":
                    var splitOneArray = value.split(";"), splitTwoArray = value.split("\n"), tags = (value.split(","), 
                    _.chain([ splitOneArray, splitTwoArray ]).flatten().map(function(tagName) {
                        return tagName.trim();
                    }).compact().uniq().map(function(tagName) {
                        return tagKey + " - " + tagName;
                    }).value());
                    return tags;

                  case "key":
                    var string = String(value).toLowerCase();
                    switch (string) {
                      case "y":
                      case "1":
                      case "yes":
                      case "true":
                        return tagKey;

                      default:
                        return;
                    }
                }
            }).flattenDeep().compact().value(), detailSheets = _.chain(activeMaps.definitions).map(function(entry, definitionName) {
                var object = {
                    _external: contact._id + "-" + definitionName,
                    contact: contact._id,
                    title: contact.title + " " + definitionName,
                    _type: "contactdetail",
                    definition: definitionName,
                    data: {},
                    realms: contact.realms
                }, plucked = _.chain(entry).filter(function(field) {
                    var pluckedValue = _.get(contact, "data." + field.from);
                    return pluckedValue ? !0 : void 0;
                }).map(function(field) {
                    var pluckedValue = _.get(contact, "data." + field.from), toField = field.to;
                    toField && toField.length || (toField = _.camelCase(field.from));
                    var toPath = "data." + toField;
                    switch (field.type) {
                      case "boolean":
                        var asString = String(pluckedValue).toLowerCase();
                        switch (asString) {
                          case "true":
                          case "y":
                          case "yes":
                            pluckedValue = !0;
                            break;

                          case "false":
                          case "n":
                          case "no":
                            pluckedValue = !1;
                            break;

                          default:
                            pluckedValue = !0;
                        }
                    }
                    console.log("Save object for " + contact.title, toPath, pluckedValue), _.set(object, toPath, pluckedValue);
                }).value();
                return plucked.length ? object : void 0;
            }).compact().flattenDeep().compact().value(), hasDetailSheets = detailSheets.length, hasTeamsToJoin = teams.length, hasTagsToAdd = tags.length;
            return hasDetailSheets || hasTeamsToJoin || hasTagsToAdd ? void setTimeout(function() {
                async.series([ createSheets, joinTeams, addTags ], next);
            }, 100) : next();
        }, complete);
    };
}

function LoginPageController($scope, FluroBreadcrumbService, $stateParams, $state, $http, Fluro, FluroContent, FluroTokenService) {
    $scope.credentials = {}, $scope.settings = {}, $scope.sendResetToken = function() {
        function resetSuccess(res) {
            $scope.settings.state = "complete";
        }
        function resetFailed(err) {
            $scope.settings.errorMessage = err.data.error, $scope.settings.state = "error";
        }
        $scope.settings.state = "processing";
        var resetDetails = {};
        resetDetails.email = $scope.credentials.username, resetDetails.redirect = "/reset-password";
        var request = FluroTokenService.sendResetPasswordRequest(resetDetails, {
            application: !0
        });
        request.then(resetSuccess, resetFailed);
    }, $scope.backToLogin = function() {
        $scope.settings.state = null, $state.go("login.form");
    }, $scope.login = function() {
        function loginSuccess(res) {
            if ($scope.settings.state = "complete", $stateParams["package"]) return void $state.go("package", {
                slug: $stateParams["package"]
            });
            if ($stateParams.returnTo) {
                var trailLength = FluroBreadcrumbService.trail.length, lastPage = FluroBreadcrumbService.trail[trailLength - 2];
                return FluroBreadcrumbService.backTo(lastPage);
            }
            $state.go("search");
        }
        function loginFailed(err) {
            $scope.settings.state = "ready", console.log("FAILED", err), $scope.settings.error = err.data;
        }
        if (!$scope.credentials.username || !$scope.credentials.username.length) return $scope.settings.error = "Email Address is a required field";
        if (!$scope.credentials.password || !$scope.credentials.password.length) return $scope.settings.error = "Password is a required field";
        $scope.settings.state = "processing", $scope.settings.error = null;
        var request = FluroTokenService.login($scope.credentials, {
            application: !0
        });
        request.then(loginSuccess, loginFailed);
    };
}

function MyAccountController($scope, $rootScope, purchases, posts, FluroTokenService, paymentMethods, PurchaseService, FluroContent) {
    function updatePurchases(purchaseList) {
        var cleaned = _.chain(purchaseList).map(function(purchase) {
            purchase.purchaser && (purchase.grantedBy = purchase.purchaser.name), purchase.managedPurchaser && (purchase.grantedBy = purchase.managedPurchaser.title);
            var userPersona = $rootScope.user.persona, userID = $rootScope.user._id, managedPurchaser = purchase.managedPurchaser, purchaser = purchase.purchaser;
            return managedPurchaser && managedPurchaser._id && (managedPurchaser = managedPurchaser._id), 
            purchaser && purchaser._id && (purchaser = purchaser._id), purchase.owned = !0, 
            purchaser && purchaser != userID && (purchase.owned = !1), managedPurchaser && managedPurchaser != userPersona && (purchase.owned = !1), 
            purchase;
        }).value();
        $scope.ownedPurchases = _.filter(cleaned, {
            owned: !0
        }), $scope.licensedPurchases = _.filter(cleaned, {
            owned: !1
        }), $scope.purchases = cleaned;
    }
    updatePurchases(purchases), $scope.methods = paymentMethods, $scope.expanded = {}, 
    $scope.settings = {
        activeTab: "purchases",
        cacheBuster: 1
    }, 1 == $scope.ownedPurchases.length && ($scope.expanded.purchase = $scope.ownedPurchases[0]);
    var comments = [], notes = [];
    _.each(posts, function(post) {
        switch (post.definition) {
          case "vodComment":
            comments.push(post);
            break;

          case "vodNote":
            notes.push(post);
        }
    }), $scope.getParentSref = function(parent) {
        switch (parent.definition) {
          case "vodEvent":
            return "stream({slug:'" + parent.slug + "'})";

          case "vodEpisode":
            return "watchVideo({slug:'" + parent.slug + "'})";
        }
    }, $scope._proposedLicense = {}, $scope.sendInvite = function(purchase) {
        $scope._proposedLicense.processing = !0;
        var details = angular.copy($scope._proposedLicense);
        details.redirect = "/invited", details.purchase = purchase._id;
        var request = FluroTokenService.applicationRequest(details, "/invite");
        request.then(function(res) {
            purchase.managedLicense.push(res.data), $scope._proposedLicense = {}, console.log("Invitation request made", res);
        }, function(err) {
            $scope._proposedLicense.processing = !1, console.log("Invitation request failed", err);
        });
    }, $scope.licensesUsed = function(purchase) {
        var total = 0, licenses = purchase.license.length, managedLicenses = purchase.managedLicense.length;
        return total += licenses, total += managedLicenses;
    }, $scope.licensesAvailable = function(purchase) {
        var total = purchase.limit - 1, licensesUsed = $scope.licensesUsed(purchase);
        return total - licensesUsed;
    }, $scope.comments = comments, $scope.uniqueComments = _.chain(comments).uniqBy(function(comment) {
        return comment.parent._id;
    }).orderBy(function(comment) {
        var date = new Date(comment.created);
        return date.getTime();
    }).value(), $scope.notes = notes, $scope.uniqueNotes = _.chain(notes).uniqBy(function(comment) {
        return comment.parent._id;
    }).orderBy(function(comment) {
        var date = new Date(comment.created);
        return date.getTime();
    }).value(), $scope.toggleExpanded = function(type, item) {
        $scope.ownedPurchases.length > 1 && ($scope.expanded[type] == item ? $scope.expanded[type] = null : $scope.expanded[type] = item);
    }, $scope.cancelSubscription = function(purchase) {
        function cancelSuccess(res) {
            console.log("Subscription cancelled", res), FluroContent.endpoint("my/purchases", !1, !0).query({}).$promise.then(function(purchases) {
                updatePurchases(purchases);
            });
        }
        function cancelFailed(err) {
            console.log("Subscription cancel error", err);
        }
        console.log("Cancel"), PurchaseService.cancelSubscription(purchase).then(cancelSuccess, cancelFailed);
    };
}

function ResetPasswordPageController($scope, $rootScope, PurchaseService, $state, FluroTokenService, $http, user, token) {
    $scope.settings = {}, user && user._id ? $scope.persona = user : ($scope.settings.state = "error", 
    $scope.settings.errorMessage = user), $scope.token = token, $scope.update = function() {
        function resetSuccess(res) {
            $scope.settings.state = "complete", $state.go("home");
        }
        function resetFailed(err) {
            $scope.settings.state = "failed", $scope.settings.errorMessage = err.data;
        }
        $scope.settings.state = "processing";
        var request = FluroTokenService.updateUserWithToken(token, $scope.persona, {
            application: !0
        });
        request.then(resetSuccess, resetFailed);
    };
}

function SearchPageController($scope) {}

function SignupPageController($scope, $q, PurchaseService, $rootScope, Fluro, $state, $stateParams, FluroBreadcrumbService, FluroContent, FluroTokenService) {
    function redirectOnSignupComplete() {
        if ($stateParams["package"]) return $state.go("package", {
            slug: $stateParams["package"]
        });
        if ($stateParams.returnTo) {
            var trailLength = FluroBreadcrumbService.trail.length, lastPage = FluroBreadcrumbService.trail[trailLength - 2];
            return FluroBreadcrumbService.backTo(lastPage);
        }
        return $state.go("home");
    }
    $scope.credentials = {}, $scope.settings = {}, $scope.signup = function() {
        function signupSuccess(res) {
            console.log("Signup success!!");
            var freeProductIDs = _.chain($rootScope.applicationData.collectOnArrival).filter(function(product) {
                return 0 == Number(product.amount);
            }).map(function(product) {
                return product._id;
            }).value();
            return freeProductIDs && freeProductIDs.length ? void PurchaseService.collectFreeProducts(freeProductIDs).then(function(res) {
                return redirectOnSignupComplete();
            }, function(err) {
                return redirectOnSignupComplete();
            }) : redirectOnSignupComplete();
        }
        function signupFailed(err) {
            if ($scope.settings.state = "ready", console.log("FAILED", err), err.data.errors) {
                var errors = _.map(err.data.errors, function(err) {
                    return err.message;
                });
                return $scope.settings.error = errors[0];
            }
            $scope.settings.error = err.data;
        }
        if (!$scope.credentials.firstName || !$scope.credentials.firstName.length) return $scope.settings.error = "First Name is a required field";
        if (!$scope.credentials.lastName || !$scope.credentials.lastName.length) return $scope.settings.error = "Last Name is a required field";
        if (!$scope.credentials.username || !$scope.credentials.username.length) return $scope.settings.error = "Email Address is a required field";
        if (!$scope.credentials.password || !$scope.credentials.password.length) return $scope.settings.error = "Password is a required field";
        if (!$scope.credentials.confirmPassword || !$scope.credentials.confirmPassword.length) return $scope.settings.error = "Please confirm your password";
        if ($scope.credentials.confirmPassword != $scope.credentials.password) return $scope.settings.error = "Your password and confirm password do not match";
        $scope.settings.state = "processing", $scope.settings.error = null;
        var request = FluroTokenService.signup($scope.credentials, {
            application: !0
        });
        request.then(signupSuccess, signupFailed);
    };
}

var app = angular.module("fluro", [ "fluro.boilerplate" ]);

app.config(function($stateProvider) {
    $stateProvider.state("home", {
        url: "/",
        templateUrl: "routes/home/home.html",
        controller: HomeController,
        data: HomeController.data,
        resolve: HomeController.resolve
    });
});

var boilerplate = angular.module("fluro.boilerplate", [ "ngAnimate", "ngResource", "ui.router", "ngTouch", "fluro.config", "fluro.access", "fluro.validate", "fluro.interactions", "fluro.content", "fluro.asset", "fluro.socket", "fluro.video", "angular.filter", "formly", "ui.bootstrap", "formlyBootstrap", "slickCarousel", "angulartics", "angulartics.google.analytics" ]);

boilerplate.config(function($stateProvider, $compileProvider, $httpProvider, FluroProvider, $urlRouterProvider, $locationProvider, $analyticsProvider) {
    $analyticsProvider.settings.ga.additionalAccountNames = [ "fluro" ], $analyticsProvider.settings.ga.additionalAccountHitTypes.setUserProperties = !0, 
    $analyticsProvider.settings.ga.additionalAccountHitTypes.userTiming = !0, $compileProvider.debugInfoEnabled(!1);
    var accessToken = getMetaKey("fluro_application_key"), apiURL = getMetaKey("fluro_url"), initialConfig = {
        apiURL: apiURL,
        token: accessToken,
        sessionStorage: !1,
        backupToken: accessToken
    }, appDevelopmentURL = getMetaKey("app_dev_url");
    appDevelopmentURL && appDevelopmentURL.length ? initialConfig.appDevelopmentURL = appDevelopmentURL : $locationProvider.html5Mode(!0), 
    FluroProvider.set(initialConfig), accessToken || ($httpProvider.defaults.withCredentials = !0), 
    $httpProvider.interceptors.push("FluroAuthentication"), $urlRouterProvider.otherwise("/");
}), boilerplate.run(function($rootScope, $sessionStorage, PurchaseService, Asset, FluroTokenService, FluroSEOService, FluroContent, FluroBreadcrumbService, FluroScrollService, $location, $timeout, $state, $analytics) {
    if ($rootScope.asset = Asset, $rootScope.$state = $state, $rootScope.session = $sessionStorage, 
    $rootScope.breadcrumb = FluroBreadcrumbService, $rootScope.purchaseService = PurchaseService, 
    $rootScope.scroll = FluroScrollService, $rootScope.seo = FluroSEOService, applicationData) {
        $rootScope.applicationData = applicationData, applicationUser && applicationUser._id && ($rootScope.user = applicationUser, 
        $analytics.setUsername(applicationUser._id), $analytics.setAlias(applicationUser.email), 
        $analytics.setUserProperties({
            dimension1: applicationUser.account._id
        }));
        var siteName = _.get(applicationData, "_application.title");
        siteName && siteName.length && (FluroSEOService.siteTitle = siteName);
        var defaultImageID = _.get(applicationData, "publicData.seoImage"), applicationID = _.get(applicationData, "_application._id");
        defaultImageID && (defaultImageID._id && (defaultImageID = defaultImageID._id), 
        FluroSEOService.defaultImageURL = Asset.imageUrl(defaultImageID, 640, null, {
            from: applicationID,
            extension: "jpg"
        }));
        var defaultSEODescription = _.get(applicationData, "publicData.seoDescription");
        FluroSEOService.defaultDescription = defaultSEODescription, $rootScope.logout = function() {
            FluroTokenService.logout(), $rootScope.sidebarExpanded = !1, $state.go("home");
        }, $rootScope.$watch("user", function(user) {
            PurchaseService.refreshPurchases();
        }), $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
            $rootScope.sidebarExpanded = !1;
            var isLoggedIn = $rootScope.user;
            if (toState.data) {
                if (toState.data.denyAuthenticated && isLoggedIn) return event.preventDefault(), 
                void $state.go("home");
                if (toState.data.requireLogin && !isLoggedIn) return event.preventDefault(), $state.go("login.form"), 
                void $rootScope.$broadcast("$preloaderHide");
            }
        }), $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
            throw error;
        }), $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams, error) {
            $rootScope.currentState = toState.name;
        }), FastClick.attach(document.body);
    }
}), app.directive("dateselect", function($document) {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: "admin-date-select/admin-date-select.html",
        scope: {
            boundModel: "=ngModel",
            label: "=ngLabel",
            minDate: "=minDate",
            initDate: "=initDate",
            useTime: "=useTime",
            required: "=",
            rounding: "=",
            forceDate: "="
        },
        link: function($scope, element, attr) {
            function elementClick(event) {
                event.stopPropagation();
            }
            function documentClick(event) {
                $scope.$apply(function() {
                    $scope.open = !1;
                });
            }
            $scope.$watch("settings.open", function(bol) {
                bol ? (element.on("click", elementClick), $document.on("click", documentClick)) : (element.off("click", elementClick), 
                $document.off("click", documentClick));
            });
        },
        controller: function($scope, $timeout) {
            function updateLabel() {
                if ($scope.boundModel) {
                    var date = new Date($scope.boundModel);
                    $scope.useTime ? $scope.readable = date.format("D j F g:i") + '<span class="meridian">' + date.format("a") + "</span>" : $scope.readable = date.format("D j F");
                } else $scope.label ? $scope.readable = $scope.label : $scope.readable = "None provided";
            }
            function boundModelChanged() {
                $scope.settings.dateModel != $scope.boundModel && ($scope.settings.dateModel = $scope.boundModel = new Date($scope.boundModel)), 
                updateLabel();
            }
            function dateModelChanged() {
                $scope.boundModel != $scope.settings.dateModel && ($scope.boundModel = $scope.settings.dateModel = new Date($scope.settings.dateModel)), 
                updateLabel();
            }
            $scope.settings = {
                dateModel: new Date()
            }, $scope.forceDate && !$scope.boundModel && ($scope.boundModel = new Date());
            var coeff = 3e5;
            $scope.removeDate = function() {
                $scope.boundModel = null;
            }, $scope.rounding && _.isDate($scope.boundModel) && ($scope.boundModel = new Date(Math.round($scope.boundModel.getTime() / coeff) * coeff)), 
            $scope.$watch("boundModel", boundModelChanged, !0), $scope.$watch("settings.dateModel", dateModelChanged, !0);
        }
    };
}), app.directive("personaImageReplaceForm", function(Fluro, $http, $timeout) {
    return {
        restrict: "E",
        scope: {
            personaID: "=ngModel",
            cacheBuster: "=ngCache",
            resetToken: "=?resetToken"
        },
        templateUrl: "admin-persona-image-replace/persona-image-replace-form.html",
        link: function($scope, $element, $attr) {
            function uploadComplete(res) {
                $scope.settings.state = "complete", console.log("uploadCompleted", res), $timeout(function() {
                    $scope.cacheBuster || ($scope.cacheBuster = 1), $scope.cacheBuster++;
                });
            }
            function uploadFailed(err) {
                $scope.settings.state = "error", $scope.settings.errorMessage = err.data, console.log("UPLOAD FAILED", err);
            }
            function fileSelected(event) {
                $scope.settings.state = "processing";
                var formData = new FormData();
                formData.append("file", fileInput[0].files[0]);
                var uploadURL = Fluro.apiURL + "/persona/" + $scope.personaID + "/image";
                $scope.resetToken && $scope.resetToken.length && (uploadURL += "?resetToken=" + $scope.resetToken), 
                $http({
                    url: uploadURL,
                    method: "POST",
                    data: formData,
                    headers: {
                        "Content-Type": void 0
                    }
                }).then(uploadComplete, uploadFailed);
            }
            $scope.settings = {
                state: "ready"
            };
            var fileInput;
            $scope.$watch("settings.state", function(state) {
                fileInput && fileInput.off("change"), fileInput = $element.find("#file"), fileInput.on("change", fileSelected), 
                console.log("Found File Input!");
            }), $scope.$on("$destroy", function() {
                fileInput && fileInput.off("change");
            });
        }
    };
}), function() {
    Date.shortMonths = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], 
    Date.longMonths = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ], 
    Date.shortDays = [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], Date.longDays = [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ];
    var replaceChars = {
        d: function() {
            return (this.getDate() < 10 ? "0" : "") + this.getDate();
        },
        D: function() {
            return Date.shortDays[this.getDay()];
        },
        j: function() {
            return this.getDate();
        },
        l: function() {
            return Date.longDays[this.getDay()];
        },
        N: function() {
            return 0 == this.getDay() ? 7 : this.getDay();
        },
        S: function() {
            return this.getDate() % 10 == 1 && 11 != this.getDate() ? "st" : this.getDate() % 10 == 2 && 12 != this.getDate() ? "nd" : this.getDate() % 10 == 3 && 13 != this.getDate() ? "rd" : "th";
        },
        w: function() {
            return this.getDay();
        },
        z: function() {
            var d = new Date(this.getFullYear(), 0, 1);
            return Math.ceil((this - d) / 864e5);
        },
        W: function() {
            var target = new Date(this.valueOf()), dayNr = (this.getDay() + 6) % 7;
            target.setDate(target.getDate() - dayNr + 3);
            var firstThursday = target.valueOf();
            return target.setMonth(0, 1), 4 !== target.getDay() && target.setMonth(0, 1 + (4 - target.getDay() + 7) % 7), 
            1 + Math.ceil((firstThursday - target) / 6048e5);
        },
        F: function() {
            return Date.longMonths[this.getMonth()];
        },
        m: function() {
            return (this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1);
        },
        M: function() {
            return Date.shortMonths[this.getMonth()];
        },
        n: function() {
            return this.getMonth() + 1;
        },
        t: function() {
            var d = new Date();
            return new Date(d.getFullYear(), d.getMonth(), 0).getDate();
        },
        L: function() {
            var year = this.getFullYear();
            return year % 400 == 0 || year % 100 != 0 && year % 4 == 0;
        },
        o: function() {
            var d = new Date(this.valueOf());
            return d.setDate(d.getDate() - (this.getDay() + 6) % 7 + 3), d.getFullYear();
        },
        Y: function() {
            return this.getFullYear();
        },
        y: function() {
            return ("" + this.getFullYear()).substr(2);
        },
        a: function() {
            return this.getHours() < 12 ? "am" : "pm";
        },
        A: function() {
            return this.getHours() < 12 ? "AM" : "PM";
        },
        B: function() {
            return Math.floor(1e3 * ((this.getUTCHours() + 1) % 24 + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) / 24);
        },
        g: function() {
            return this.getHours() % 12 || 12;
        },
        G: function() {
            return this.getHours();
        },
        h: function() {
            return ((this.getHours() % 12 || 12) < 10 ? "0" : "") + (this.getHours() % 12 || 12);
        },
        H: function() {
            return (this.getHours() < 10 ? "0" : "") + this.getHours();
        },
        i: function() {
            return (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
        },
        s: function() {
            return (this.getSeconds() < 10 ? "0" : "") + this.getSeconds();
        },
        u: function() {
            var m = this.getMilliseconds();
            return (10 > m ? "00" : 100 > m ? "0" : "") + m;
        },
        e: function() {
            return "Not Yet Supported";
        },
        I: function() {
            for (var DST = null, i = 0; 12 > i; ++i) {
                var d = new Date(this.getFullYear(), i, 1), offset = d.getTimezoneOffset();
                if (null === DST) DST = offset; else {
                    if (DST > offset) {
                        DST = offset;
                        break;
                    }
                    if (offset > DST) break;
                }
            }
            return this.getTimezoneOffset() == DST | 0;
        },
        O: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + "00";
        },
        P: function() {
            return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + ":00";
        },
        T: function() {
            return this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
        },
        Z: function() {
            return 60 * -this.getTimezoneOffset();
        },
        c: function() {
            return this.format("Y-m-d\\TH:i:sP");
        },
        r: function() {
            return this.toString();
        },
        U: function() {
            return this.getTime() / 1e3;
        }
    };
    Date.prototype.format = function(format) {
        var date = this;
        return format.replace(/(\\?)(.)/g, function(_, esc, chr) {
            return "" === esc && replaceChars[chr] ? replaceChars[chr].call(date) : chr;
        });
    };
}.call(this), function() {
    "use strict";
    function FastClick(layer, options) {
        function bind(method, context) {
            return function() {
                return method.apply(context, arguments);
            };
        }
        var oldOnClick;
        if (options = options || {}, this.trackingClick = !1, this.trackingClickStart = 0, 
        this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, 
        this.touchBoundary = options.touchBoundary || 10, this.layer = layer, this.tapDelay = options.tapDelay || 200, 
        !FastClick.notNeeded(layer)) {
            for (var methods = [ "onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel" ], context = this, i = 0, l = methods.length; l > i; i++) context[methods[i]] = bind(context[methods[i]], context);
            deviceIsAndroid && (layer.addEventListener("mouseover", this.onMouse, !0), layer.addEventListener("mousedown", this.onMouse, !0), 
            layer.addEventListener("mouseup", this.onMouse, !0)), layer.addEventListener("click", this.onClick, !0), 
            layer.addEventListener("touchstart", this.onTouchStart, !1), layer.addEventListener("touchmove", this.onTouchMove, !1), 
            layer.addEventListener("touchend", this.onTouchEnd, !1), layer.addEventListener("touchcancel", this.onTouchCancel, !1), 
            Event.prototype.stopImmediatePropagation || (layer.removeEventListener = function(type, callback, capture) {
                var rmv = Node.prototype.removeEventListener;
                "click" === type ? rmv.call(layer, type, callback.hijacked || callback, capture) : rmv.call(layer, type, callback, capture);
            }, layer.addEventListener = function(type, callback, capture) {
                var adv = Node.prototype.addEventListener;
                "click" === type ? adv.call(layer, type, callback.hijacked || (callback.hijacked = function(event) {
                    event.propagationStopped || callback(event);
                }), capture) : adv.call(layer, type, callback, capture);
            }), "function" == typeof layer.onclick && (oldOnClick = layer.onclick, layer.addEventListener("click", function(event) {
                oldOnClick(event);
            }, !1), layer.onclick = null);
        }
    }
    var deviceIsAndroid = navigator.userAgent.indexOf("Android") > 0, deviceIsIOS = /iP(ad|hone|od)/.test(navigator.userAgent), deviceIsIOS4 = deviceIsIOS && /OS 4_\d(_\d)?/.test(navigator.userAgent), deviceIsIOSWithBadTarget = deviceIsIOS && /OS ([6-9]|\d{2})_\d/.test(navigator.userAgent), deviceIsBlackBerry10 = navigator.userAgent.indexOf("BB10") > 0;
    FastClick.prototype.needsClick = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "button":
          case "select":
          case "textarea":
            if (target.disabled) return !0;
            break;

          case "input":
            if (deviceIsIOS && "file" === target.type || target.disabled) return !0;
            break;

          case "label":
          case "video":
            return !0;
        }
        return /\bneedsclick\b/.test(target.className);
    }, FastClick.prototype.needsFocus = function(target) {
        switch (target.nodeName.toLowerCase()) {
          case "textarea":
            return !0;

          case "select":
            return !deviceIsAndroid;

          case "input":
            switch (target.type) {
              case "button":
              case "checkbox":
              case "file":
              case "image":
              case "radio":
              case "submit":
                return !1;
            }
            return !target.disabled && !target.readOnly;

          default:
            return /\bneedsfocus\b/.test(target.className);
        }
    }, FastClick.prototype.sendClick = function(targetElement, event) {
        var clickEvent, touch;
        document.activeElement && document.activeElement !== targetElement && document.activeElement.blur(), 
        touch = event.changedTouches[0], clickEvent = document.createEvent("MouseEvents"), 
        clickEvent.initMouseEvent(this.determineEventType(targetElement), !0, !0, window, 1, touch.screenX, touch.screenY, touch.clientX, touch.clientY, !1, !1, !1, !1, 0, null), 
        clickEvent.forwardedTouchEvent = !0, targetElement.dispatchEvent(clickEvent);
    }, FastClick.prototype.determineEventType = function(targetElement) {
        return deviceIsAndroid && "select" === targetElement.tagName.toLowerCase() ? "mousedown" : "click";
    }, FastClick.prototype.focus = function(targetElement) {
        var length;
        deviceIsIOS && targetElement.setSelectionRange && 0 !== targetElement.type.indexOf("date") && "time" !== targetElement.type && "month" !== targetElement.type ? (length = targetElement.value.length, 
        targetElement.setSelectionRange(length, length)) : targetElement.focus();
    }, FastClick.prototype.updateScrollParent = function(targetElement) {
        var scrollParent, parentElement;
        if (scrollParent = targetElement.fastClickScrollParent, !scrollParent || !scrollParent.contains(targetElement)) {
            parentElement = targetElement;
            do {
                if (parentElement.scrollHeight > parentElement.offsetHeight) {
                    scrollParent = parentElement, targetElement.fastClickScrollParent = parentElement;
                    break;
                }
                parentElement = parentElement.parentElement;
            } while (parentElement);
        }
        scrollParent && (scrollParent.fastClickLastScrollTop = scrollParent.scrollTop);
    }, FastClick.prototype.getTargetElementFromEventTarget = function(eventTarget) {
        return eventTarget.nodeType === Node.TEXT_NODE ? eventTarget.parentNode : eventTarget;
    }, FastClick.prototype.onTouchStart = function(event) {
        var targetElement, touch, selection;
        if (event.targetTouches.length > 1) return !0;
        if (targetElement = this.getTargetElementFromEventTarget(event.target), touch = event.targetTouches[0], 
        deviceIsIOS) {
            if (selection = window.getSelection(), selection.rangeCount && !selection.isCollapsed) return !0;
            if (!deviceIsIOS4) {
                if (touch.identifier && touch.identifier === this.lastTouchIdentifier) return event.preventDefault(), 
                !1;
                this.lastTouchIdentifier = touch.identifier, this.updateScrollParent(targetElement);
            }
        }
        return this.trackingClick = !0, this.trackingClickStart = event.timeStamp, this.targetElement = targetElement, 
        this.touchStartX = touch.pageX, this.touchStartY = touch.pageY, event.timeStamp - this.lastClickTime < this.tapDelay && event.preventDefault(), 
        !0;
    }, FastClick.prototype.touchHasMoved = function(event) {
        var touch = event.changedTouches[0], boundary = this.touchBoundary;
        return Math.abs(touch.pageX - this.touchStartX) > boundary || Math.abs(touch.pageY - this.touchStartY) > boundary ? !0 : !1;
    }, FastClick.prototype.onTouchMove = function(event) {
        return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(event.target) || this.touchHasMoved(event)) && (this.trackingClick = !1, 
        this.targetElement = null), !0) : !0;
    }, FastClick.prototype.findControl = function(labelElement) {
        return void 0 !== labelElement.control ? labelElement.control : labelElement.htmlFor ? document.getElementById(labelElement.htmlFor) : labelElement.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea");
    }, FastClick.prototype.onTouchEnd = function(event) {
        var forElement, trackingClickStart, targetTagName, scrollParent, touch, targetElement = this.targetElement;
        if (!this.trackingClick) return !0;
        if (event.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, 
        !0;
        if (this.cancelNextClick = !1, this.lastClickTime = event.timeStamp, trackingClickStart = this.trackingClickStart, 
        this.trackingClick = !1, this.trackingClickStart = 0, deviceIsIOSWithBadTarget && (touch = event.changedTouches[0], 
        targetElement = document.elementFromPoint(touch.pageX - window.pageXOffset, touch.pageY - window.pageYOffset) || targetElement, 
        targetElement.fastClickScrollParent = this.targetElement.fastClickScrollParent), 
        targetTagName = targetElement.tagName.toLowerCase(), "label" === targetTagName) {
            if (forElement = this.findControl(targetElement)) {
                if (this.focus(targetElement), deviceIsAndroid) return !1;
                targetElement = forElement;
            }
        } else if (this.needsFocus(targetElement)) return event.timeStamp - trackingClickStart > 100 || deviceIsIOS && window.top !== window && "input" === targetTagName ? (this.targetElement = null, 
        !1) : (this.focus(targetElement), this.sendClick(targetElement, event), deviceIsIOS && "select" === targetTagName || (this.targetElement = null, 
        event.preventDefault()), !1);
        return deviceIsIOS && !deviceIsIOS4 && (scrollParent = targetElement.fastClickScrollParent, 
        scrollParent && scrollParent.fastClickLastScrollTop !== scrollParent.scrollTop) ? !0 : (this.needsClick(targetElement) || (event.preventDefault(), 
        this.sendClick(targetElement, event)), !1);
    }, FastClick.prototype.onTouchCancel = function() {
        this.trackingClick = !1, this.targetElement = null;
    }, FastClick.prototype.onMouse = function(event) {
        return this.targetElement ? event.forwardedTouchEvent ? !0 : event.cancelable && (!this.needsClick(this.targetElement) || this.cancelNextClick) ? (event.stopImmediatePropagation ? event.stopImmediatePropagation() : event.propagationStopped = !0, 
        event.stopPropagation(), event.preventDefault(), !1) : !0 : !0;
    }, FastClick.prototype.onClick = function(event) {
        var permitted;
        return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, 
        !0) : "submit" === event.target.type && 0 === event.detail ? !0 : (permitted = this.onMouse(event), 
        permitted || (this.targetElement = null), permitted);
    }, FastClick.prototype.destroy = function() {
        var layer = this.layer;
        deviceIsAndroid && (layer.removeEventListener("mouseover", this.onMouse, !0), layer.removeEventListener("mousedown", this.onMouse, !0), 
        layer.removeEventListener("mouseup", this.onMouse, !0)), layer.removeEventListener("click", this.onClick, !0), 
        layer.removeEventListener("touchstart", this.onTouchStart, !1), layer.removeEventListener("touchmove", this.onTouchMove, !1), 
        layer.removeEventListener("touchend", this.onTouchEnd, !1), layer.removeEventListener("touchcancel", this.onTouchCancel, !1);
    }, FastClick.notNeeded = function(layer) {
        var metaViewport, chromeVersion, blackberryVersion;
        if ("undefined" == typeof window.ontouchstart) return !0;
        if (chromeVersion = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [ , 0 ])[1]) {
            if (!deviceIsAndroid) return !0;
            if (metaViewport = document.querySelector("meta[name=viewport]")) {
                if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
                if (chromeVersion > 31 && document.documentElement.scrollWidth <= window.outerWidth) return !0;
            }
        }
        if (deviceIsBlackBerry10 && (blackberryVersion = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), 
        blackberryVersion[1] >= 10 && blackberryVersion[2] >= 3 && (metaViewport = document.querySelector("meta[name=viewport]")))) {
            if (-1 !== metaViewport.content.indexOf("user-scalable=no")) return !0;
            if (document.documentElement.scrollWidth <= window.outerWidth) return !0;
        }
        return "none" === layer.style.msTouchAction ? !0 : !1;
    }, FastClick.attach = function(layer, options) {
        return new FastClick(layer, options);
    }, "function" == typeof define && "object" == typeof define.amd && define.amd ? define(function() {
        return FastClick;
    }) : "undefined" != typeof module && module.exports ? (module.exports = FastClick.attach, 
    module.exports.FastClick = FastClick) : window.FastClick = FastClick;
}(), app.directive("extendedFieldRender", function($compile, $templateCache) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            field: "=ngField",
            model: "=ngModel"
        },
        templateUrl: "extended-field-render/extended-field-render.html",
        link: function($scope, $element, $attrs) {
            $scope.showField = !0;
            var template = "";
            switch ($scope.field.type) {
              case "void":
              case "null":
              case "":
                return $element.empty();
            }
            if ("group" == $scope.field.type ? template = $scope.field.asObject ? _.isArray($scope.model[$scope.field.key]) ? '<div ng-repeat="group in model[field.key]" class="panel panel-default"><div class="panel-heading">{{field.title}} {{$index + 1}}</div><div class="panel-body"><extended-field-render ng-model="group" ng-field="subField" ng-repeat="subField in field.fields"/></div></div>' : '<extended-field-render ng-model="model[field.key]" ng-field="subField" ng-repeat="subField in field.fields"/>' : '<extended-field-render ng-model="model" ng-field="subField" ng-repeat="subField in field.fields"/>' : _.isArray($scope.model[$scope.field.key]) && $scope.model[$scope.field.key].length ? template = $templateCache.get("extended-field-render/field-types/multiple-value.html") : $scope.model[$scope.field.key] && !_.isArray($scope.model[$scope.field.key]) && (template = $templateCache.get("extended-field-render/field-types/value.html")), 
            template.length) {
                var cTemplate = $compile(template)($scope), contentHolder = $element.find("[field-transclude]");
                "group" == $scope.field.type ? contentHolder.addClass($scope.field.className).append(cTemplate) : ($element.addClass($scope.field.className), 
                contentHolder.replaceWith(cTemplate));
            } else $scope.showField = !1, $element.empty();
        }
    };
}), app.directive("extendedFields", function($compile) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs) {
            $scope.definition && ($scope.flattenedFields = getFlattenedFields($scope.definition.fields));
            var template = '<field-edit-render ng-model="item.data[field.key]" ng-field="field" ng-repeat="field in flattenedFields"></field-edit-render>', cTemplate = $compile(template)($scope);
            $element.append(cTemplate);
        }
    };
}), app.directive("viewExtendedFields", function($compile) {
    return {
        restrict: "A",
        scope: {
            item: "=",
            definition: "="
        },
        link: function($scope, $element, $attrs) {
            if ($scope.definition) {
                $scope.fields = $scope.definition.fields, console.log("what are the fields?", $scope.fields), 
                console.log("current definition", $scope.definition);
                var template = '<extended-field-render ng-model="item.data" ng-field="field" ng-repeat="field in fields"></extended-field-render>', cTemplate = $compile(template)($scope);
                $element.append(cTemplate);
            }
        }
    };
}), app.directive("extendedFields", function($compile) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs) {
            $scope.definition && ($scope.flattenedFields = getFlattenedFields($scope.definition.fields));
            var template = '<field-edit-render ng-model="item.data[field.key]" ng-field="field" ng-repeat="field in flattenedFields"></field-edit-render>', cTemplate = $compile(template)($scope);
            $element.append(cTemplate);
        }
    };
}), app.directive("fieldViewRender", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            field: "=ngField",
            model: "=ngModel"
        },
        templateUrl: "views/ui/field-view-render.html",
        controller: function($scope, ModalService) {
            $scope.viewInModal = function(item) {
                console.log("View in modal", item), ModalService.view(item);
            }, $scope.editInModal = function(item) {
                console.log("Edit in modal", item), ModalService.edit(item);
            }, _.isArray($scope.model) && ($scope.multiple = !0), 1 == $scope.field.minimum && 1 == $scope.field.maximum ? $scope.viewModel = [ $scope.model ] : $scope.viewModel = $scope.model;
        }
    };
}), app.directive("fieldObjectRender", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            model: "=ngModel"
        },
        link: function($scope) {
            $scope.create = function() {
                $scope.model || ($scope.model = {});
            };
        },
        template: '<div><pre>{{model | json}}</pre><a class="btn btn-default" ng-click="create()" ng-if="!model"><span>Add</span><i class="fa fa-plus"></i></a><div ng-if="model"><json-editor config="model"/></div></div>'
    };
}), app.directive("fieldEditRender", function($compile) {
    return {
        restrict: "E",
        replace: !0,
        scope: {
            field: "=ngField",
            model: "=ngModel"
        },
        link: function($scope, $element, $attrs) {
            var template = '<div class="form-group"><label>{{field.title}}</label><input ng-model="model" class="form-control" placeholder="{{field.title}}"></div>';
            $scope.field.params ? $scope.config = $scope.field.params : $scope.config = {}, 
            $scope.config.restrictType && ($scope.config.type = $scope.config.restrictType), 
            $scope.config.minimum = $scope.field.minimum, $scope.config.maximum = $scope.field.maximum;
            var renderName = $scope.field.directive;
            switch ($scope.field.type) {
              case "reference":
                $scope.config.allowedValues = $scope.field.allowedReferences, $scope.config.defaultValues = $scope.field.defaultReferences, 
                $scope.config.canCreate = !0, renderName = "content-select";
                break;

              default:
                $scope.config.allowedValues = $scope.field.allowedValues, $scope.config.defaultValues = $scope.field.defaultValues;
            }
            var attributes = "";
            switch ($scope.field.type) {
              case "boolean":
                attributes = 'type="checkbox" ';
                break;

              case "float":
              case "integer":
              case "number":
                attributes = 'type="number" ';
                break;

              case "email":
                attributes = 'type="email" ';
                break;

              case "date":
                attributes = 'type="date" ';
                break;

              case "reference":
              case "string":
                attributes = 'type="text" ';
                break;

              case "object":
                renderName = "field-object-render";
                break;

              case "void":
                return;
            }
            switch (renderName || (renderName = "input"), "date-select" == renderName && (renderName = "dateselect"), 
            renderName) {
              case "input":
                template = "boolean" == $scope.field.type ? '<div class="form-group"><div class="checkbox"><label><' + renderName + " " + attributes + ' ng-model="model"/>{{field.title}}</label></div></div>' : '<div class="form-group"><label>{{field.title}}</label><' + renderName + " " + attributes + ' ng-model="model" placeholder="{{field.title}}" class="form-control" ng-params="config"/></div>';
                break;

              case "textarea":
                template = '<div class="form-group"><label>{{field.title}}</label><' + renderName + " " + attributes + ' ng-model="model" placeholder="{{field.title}}" class="form-control" ng-params="config"/></div>';
                break;

              case "select":
                template = '<div class="form-group"><label>{{field.title}}</label><select ' + attributes + ' ng-model="model" class="form-control" ng-params="config">', 
                _.each($scope.field.options, function(option) {
                    template += '<option value="' + option.value + '">' + option.name + "</option>";
                }), template += "</select></div>";
                break;

              default:
                template = '<div class="form-group"><label>{{field.title}}</label><' + renderName + " " + attributes + ' ng-model="model" ng-params="config"/></div>';
            }
            if (template && template.length) {
                var cTemplate = $compile(template)($scope);
                $element.replaceWith(cTemplate);
            }
        }
    };
}), app.service("FluroBreadcrumbService", function($rootScope, $timeout, $state) {
    var backButtonPress, controller = {
        trail: []
    }, scrollPositions = {};
    return controller.trail = [], $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
        var path = $state.href(fromState, fromParams), previousScrollPosition = document.body.scrollTop;
        scrollPositions[path] = previousScrollPosition;
    }), $rootScope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams, error) {
        if ("home" == toState.name && (controller.trail.length = 0), backButtonPress) {
            var path = $state.href(toState, toParams), previousScrollPosition = scrollPositions[path];
            previousScrollPosition ? $timeout(function() {
                document.body.scrollTop = previousScrollPosition;
            }) : document.body.scrollTop = 0, controller.trail.pop(), controller.trail.pop(), 
            backButtonPress = !1;
        } else document.body.scrollTop = 0;
        controller.trail.push({
            name: toState.name,
            params: toParams
        });
    }), controller.topState = _.find($state.get(), function(state) {
        return state.name && state.name.length;
    }), controller.top = function() {
        controller.trail.length = 0, controller.topState && $state.go(controller.topState);
    }, controller.clear = function() {
        controller.trail.length = 0;
    }, controller.backTo = function(breadcrumbItem) {
        var index = controller.trail.indexOf(breadcrumbItem);
        return -1 != index ? (controller.trail.length = index, void $state.go(breadcrumbItem.name, breadcrumbItem.params)) : void 0;
    }, controller.back = function() {
        if (controller.trail.length) {
            backButtonPress = !0;
            var count = controller.trail.length, previousState = controller.trail[count - 2];
            previousState ? $state.go(previousState.name, previousState.params) : $state.$current.parent && $state.$current.parent.self.name.length ? $state.go("^") : $state.go(controller.topState);
        }
    }, controller;
}), app.directive("filterBlock", function() {
    return {
        restrict: "E",
        replace: !0,
        templateUrl: "fluro-filter-block/fluro-filter-block.html",
        scope: {
            title: "@",
            target: "=",
            items: "=",
            getpath: "@",
            setpath: "@",
            specific: "=?"
        },
        controller: "FluroFilterBlockController",
        link: function(scope, element, attrs) {}
    };
}), app.controller("FluroFilterBlockController", function($scope) {
    $scope.target || ($scope.target = {}), $scope.settings = {}, $scope.specific ? $scope.filterItems = $scope.specific : $scope.filterItems = [], 
    $scope.clicked = function() {
        $scope.settings.expanded = !$scope.settings.expanded;
    }, $scope.getTitle = function() {
        var selected = _.get($scope.target, $scope.setpath);
        return selected ? selected.title : $scope.title;
    }, $scope.setFilter = function(value) {
        _.set($scope.target, $scope.setpath, value), console.log("Collapse"), $scope.settings.expanded = !1;
    }, $scope.isExpanded = function() {
        return $scope.settings.expanded ? !0 : !1;
    }, $scope.toggleFilter = function(value) {
        var selected = $scope.isActiveFilter(value);
        selected ? $scope.setFilter() : $scope.setFilter(value);
    }, $scope.isActiveFilter = function(value) {
        var selected = _.get($scope.target, $scope.setpath);
        return value && selected ? selected._id == value._id : value == selected;
    }, $scope.hasFilters = function() {
        var existingFilter = _.get($scope.target, $scope.setpath);
        return existingFilter ? !0 : void 0;
    }, $scope.$watch("items", function(items) {
        return items && items.length ? $scope.specific ? $scope.filterItems = $scope.specific : void ($scope.filterItems = _.chain(items).map(function(item) {
            var values = _.get(item, $scope.getpath);
            return values;
        }).flattenDeep().compact().uniqBy(function(option) {
            return option._id;
        }).orderBy(function(option) {
            return option.title;
        }).value()) : $scope.filterItems = [];
    });
}), app.directive("floatLabel", function() {
    return {
        restrict: "A",
        scope: !0,
        compile: function($element, $attrs) {
            function generateNgModelKey(inputBox) {
                var inputId = inputBox.attr("id") || "", inputName = inputBox.attr("name") || "";
                if (0 === inputId.length && 0 === inputName.length) throw "If no ng-model is defined, the input should have an id or a name";
                return "input_" + (inputId ? inputId : inputName);
            }
            var template, attr, templateAttributes = [];
            if (!$attrs.placeholder) throw "Floating label needs a placeholder";
            for (attr in $attrs) $attrs.hasOwnProperty(attr) && "$" !== attr.substr(0, 1) && "floatLabel" !== attr && templateAttributes.push($attrs.$attr[attr] + '="' + $attrs[attr] + '"');
            return $attrs.ngModel || templateAttributes.push('ng-model="' + generateNgModelKey($element) + '"'), 
            template = '<div class="float-label"><label ng-class="{ \'active\': showLabel }">' + $attrs.placeholder + "</label><input " + templateAttributes.join(" ") + " /></div>", 
            console.log("Replace with template"), $element.replaceWith(angular.element(template)), 
            {
                post: function($scope, $element, $attrs) {
                    var inputBox = $element.find("input"), ngModelKey = inputBox.attr("ng-model");
                    $scope.showLabel = !1, $scope.$watch(ngModelKey, function(newValue) {
                        console.log("VALUE", ngModelKey, newValue), $scope.showLabel = "string" == typeof newValue && newValue.length > 0;
                    });
                }
            };
        }
    };
}), app.directive("infinitePager", function($timeout, $sessionStorage) {
    return {
        restrict: "A",
        link: function($scope, $element, $attr) {
            var perPage = 16;
            $attr.perPage && (perPage = parseInt($attr.perPage)), $scope.pager = {
                currentPage: 0,
                limit: perPage
            }, $scope.pages = [], $scope.$watch($attr.items, function(items) {
                $scope.allItems = items, $scope.allItems && ($scope.pages.length = 0, $scope.pager.currentPage = 0, 
                $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1, 
                $scope.updateCurrentPage());
            }), $scope.updateCurrentPage = function() {
                $scope.allItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 0);
                var start = $scope.pager.currentPage * $scope.pager.limit, end = start + $scope.pager.limit, sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            };
            var timer;
            $scope.nextPage = function() {
                $scope.pager.currentPage < $scope.totalPages ? ($timeout.cancel(timer), timer = $timeout(function() {
                    $scope.pager.currentPage = $scope.pager.currentPage + 1, $scope.updateCurrentPage();
                })) : $scope.updateCurrentPage();
            };
        }
    };
}), app.controller("FluroInteractionButtonSelectController", function($scope, FluroValidate) {
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    var opts = ($scope.to, $scope.options);
    $scope.selection = {
        values: [],
        value: null
    };
    var definition = $scope.to.definition, minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.dragControlListeners = {
        orderChanged: function(event) {
            $scope.model[opts.key] = angular.copy($scope.selection.values);
        }
    }, $scope.selectBox = {}, $scope.selectUpdate = function() {
        $scope.selectBox.item && ($scope.selection.values.push($scope.selectBox.item), $scope.model[opts.key] = angular.copy($scope.selection.values));
    }, $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : $scope.selection.value = null;
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference), 
        setModel();
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "dob-select",
        templateUrl: "fluro-interaction-form/dob-select/fluro-dob-select.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "embedded",
        templateUrl: "fluro-interaction-form/embedded/fluro-embedded.html",
        controller: "FluroInteractionNestedController",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.directive("interactionForm", function($compile) {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            vm: "=?config",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "InteractionFormController",
        template: '<div class="fluro-interaction-form" ng-transclude-here />',
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.find("[ng-transclude-here]").append(clone);
            });
        }
    };
}), app.directive("webform", function($compile) {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            integration: "=ngPaymentIntegration",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback",
            linkedEvent: "=?linkedEvent"
        },
        transclude: !0,
        controller: "InteractionFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.config(function(formlyConfigProvider) {
    formlyConfigProvider.setType({
        name: "currency",
        "extends": "input",
        controller: function($scope) {},
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
        defaultOptions: {
            ngModelAttrs: {
                currencyDirective: {
                    attribute: "ng-currency"
                },
                fractionValue: {
                    attribute: "fraction",
                    bound: "fraction"
                },
                minimum: {
                    attribute: "min",
                    bound: "min"
                },
                maximum: {
                    attribute: "max",
                    bound: "max"
                }
            },
            templateOptions: {
                customAttrVal: "",
                required: !0,
                fraction: 2
            },
            validators: {
                validInput: {
                    expression: function($viewValue, $modelValue, scope) {
                        var numericValue = Number($modelValue);
                        if (isNaN(numericValue)) return !1;
                        var minimumAmount = scope.options.data.minimumAmount, maximumAmount = scope.options.data.maximumAmount;
                        return minimumAmount && minimumAmount > numericValue ? !1 : maximumAmount && numericValue > maximumAmount ? !1 : !0;
                    }
                }
            }
        }
    });
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.templateManipulators.postWrapper.push(function(template, options, scope) {
        var fluroErrorTemplate = $templateCache.get("fluro-interaction-form/field-errors.html");
        return "<div>" + template + fluroErrorTemplate + "</div>";
    }), formlyConfig.setType({
        name: "multiInput",
        templateUrl: "fluro-interaction-form/multi.html",
        defaultOptions: {
            noFormControl: !0,
            wrapper: [ "bootstrapLabel", "bootstrapHasError" ],
            templateOptions: {
                inputOptions: {
                    wrapper: null
                }
            }
        },
        controller: function($scope) {
            function copyItemOptions() {
                return angular.copy($scope.to.inputOptions);
            }
            $scope.copyItemOptions = copyItemOptions;
        }
    }), formlyConfig.setType({
        name: "payment",
        templateUrl: "fluro-interaction-form/payment/payment.html",
        defaultOptions: {
            noFormControl: !0
        }
    }), formlyConfig.setType({
        name: "custom",
        templateUrl: "fluro-interaction-form/custom.html",
        controller: "CustomInteractionFieldController",
        wrapper: [ "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "button-select",
        templateUrl: "fluro-interaction-form/button-select/fluro-button-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "date-select",
        templateUrl: "fluro-interaction-form/date-select/fluro-date-select.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "terms",
        templateUrl: "fluro-interaction-form/fluro-terms.html",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    }), formlyConfig.setType({
        name: "order-select",
        templateUrl: "fluro-interaction-form/order-select/fluro-order-select.html",
        controller: "FluroInteractionButtonSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("CustomInteractionFieldController", function($scope, FluroValidate) {
    $scope.$watch("model[options.key]", function(value) {
        value && $scope.fc && $scope.fc.$setTouched();
    }, !0);
}), app.controller("FluroDateSelectController", function($scope) {
    $scope.today = function() {
        $scope.model[$scope.options.key] = new Date();
    }, $scope.open = function($event) {
        $event.preventDefault(), $event.stopPropagation(), $scope.opened = !0;
    }, $scope.dateOptions = {
        formatYear: "yy",
        startingDay: 1
    }, $scope.formats = [ "dd/MM/yyyy" ], $scope.format = $scope.formats[0];
}), app.controller("InteractionFormController", function($scope, $q, $timeout, $rootScope, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function getAllErrorFields(array) {
        return _.chain(array).map(function(field) {
            if (field.fieldGroup && field.fieldGroup.length) return getAllErrorFields(field.fieldGroup);
            if (field.data && (field.data.fields && field.data.fields.length || field.data.dataFields && field.data.dataFields || field.data.replicatedFields && field.data.replicatedFields)) {
                var combined = [];
                return combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields), 
                combined = _.compact(combined), getAllErrorFields(combined);
            }
            return field;
        }).flatten().value();
    }
    function submitInteraction() {
        function processRequest() {
            function submissionSuccess(res) {
                $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
                $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), $formScope = $scope, 
                $scope.response = res, $scope.vm.state = "complete";
            }
            function submissionFail(res) {
                return $scope.vm.state = "error", res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                    return error.message;
                }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
            }
            delete interactionDetails._paymentCardCVN, delete interactionDetails._paymentCardExpMonth, 
            delete interactionDetails._paymentCardExpYear, delete interactionDetails._paymentCardName, 
            delete interactionDetails._paymentCardNumber, interactionDetails._paymentAmount && (paymentDetails.amount = 100 * parseFloat(interactionDetails._paymentAmount));
            var request = FluroInteraction.interact($scope.model.title, interactionKey, interactionDetails, paymentDetails, $scope.linkedEvent);
            request.then(submissionSuccess, submissionFail);
        }
        $scope.vm.state = "sending";
        var requiresPayment, allowsPayment, interactionKey = $scope.model.definitionName, interactionDetails = angular.copy($scope.vm.model), paymentConfiguration = $scope.model.paymentDetails;
        if (paymentConfiguration && (requiresPayment = paymentConfiguration.required, allowsPayment = paymentConfiguration.allow), 
        !requiresPayment && !allowsPayment) return processRequest();
        var paymentDetails = {};
        if (paymentConfiguration.allowAlternativePayments && paymentConfiguration.paymentMethods) {
            var selectedMethod = interactionDetails._paymentMethod;
            if (selectedMethod && "card" != selectedMethod) return paymentDetails.method = selectedMethod, 
            processRequest();
        }
        if (requiresPayment && !$formScope.vm.model.calculatedTotal) return processRequest();
        var paymentIntegration = $scope.integration;
        if (!paymentIntegration || !paymentIntegration.publicDetails) return paymentConfiguration.required, 
        alert("This form has not been configured properly. Please notify the administrator of this website immediately."), 
        void ($scope.vm.state = "ready");
        switch (paymentDetails.integration = paymentIntegration._id, paymentIntegration.module) {
          case "eway":
            if (!window.eCrypt) return $scope.vm.state = "ready";
            var key = paymentIntegration.publicDetails.publicKey, cardDetails = {};
            cardDetails.name = interactionDetails._paymentCardName, cardDetails.number = eCrypt.encryptValue(interactionDetails._paymentCardNumber, key), 
            cardDetails.cvc = eCrypt.encryptValue(interactionDetails._paymentCardCVN, key);
            var expiryMonth = String(interactionDetails._paymentCardExpMonth), expiryYear = String(interactionDetails._paymentCardExpYear);
            return expiryMonth.length < 1 && (expiryMonth = "0" + expiryMonth), cardDetails.exp_month = expiryMonth, 
            cardDetails.exp_year = expiryYear.slice(-2), paymentDetails.details = cardDetails, 
            processRequest();

          case "stripe":
            if (!window.Stripe) return $scope.vm.state = "ready";
            var liveKey = paymentIntegration.publicDetails.livePublicKey, sandboxKey = paymentIntegration.publicDetails.testPublicKey, key = liveKey;
            paymentIntegration.publicDetails.sandbox && (key = sandboxKey), Stripe.setPublishableKey(key);
            var cardDetails = {};
            cardDetails.name = interactionDetails._paymentCardName, cardDetails.number = interactionDetails._paymentCardNumber, 
            cardDetails.cvc = interactionDetails._paymentCardCVN, cardDetails.exp_month = interactionDetails._paymentCardExpMonth, 
            cardDetails.exp_year = interactionDetails._paymentCardExpYear, Stripe.card.createToken(cardDetails, function(status, response) {
                return response.error ? ($scope.processErrorMessages = [ response.error.message ], 
                void ($scope.vm.state = "error")) : (paymentDetails.details = response, processRequest());
            });
        }
    }
    $scope.vm || ($scope.vm = {}), $formScope = $scope, $scope.debugMode && console.log("-- DEBUG MODE --"), 
    $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.correctPermissions = !0, 
    $scope.readyToSubmit = !1, $scope.$watch("vm.modelForm.$invalid + vm.modelForm.$error", function() {
        var interactionForm = $scope.vm.modelForm;
        if (!interactionForm) return $scope.readyToSubmit = !1;
        if (interactionForm.$invalid) return $scope.readyToSubmit = !1;
        if (interactionForm.$error) {
            if (interactionForm.$error.required && interactionForm.$error.required.length) return $scope.readyToSubmit = !1;
            if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return $scope.readyToSubmit = !1;
        }
        $scope.readyToSubmit = !0;
    }, !0), formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), $scope.response = null, 
        $scope.vm.state = "ready", $scope.$broadcast("form-reset");
    }, $scope.$watch("model", function(newData, oldData) {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.directive) {
                  case "reference-select":
                  case "value-select":
                    newField.type = "button-select";
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive;
                }
                switch (fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "custom" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    "integer" == fieldDefinition.type && (templateOptions.type = "number", templateOptions.baseDefaultValue = 0, 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrpattern = "[0-9]*", templateOptions.customAttrinputmode = "numeric", 
                    fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        replicatedFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions.hide = fieldDefinition.hideExpression);
                    var allExpressions = _.values(fieldDefinition.expressions).join("+");
                    newField.watcher = {
                        expression: function(field, scope) {
                            return $parse(allExpressions)(scope);
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            scope.interaction || (scope.interaction = $scope.vm.model), _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(scope), fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = retrievedValue;

                                  case "hide":
                                    return field.hide = retrievedValue;
                                }
                            });
                        }
                    };
                }
                fieldDefinition.hideExpression && (newField.hideExpression = fieldDefinition.hideExpression), 
                newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if ($scope.model && "interaction" == $scope.model.parentType) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
            $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitInteraction, 
            $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
            var interactionFormSettings = $scope.model.data;
            if (interactionFormSettings || (interactionFormSettings = {}), !interactionFormSettings.allowAnonymous && !interactionFormSettings.disableDefaultFields) switch (interactionFormSettings.requireFirstName = !0, 
            interactionFormSettings.requireLastName = !0, interactionFormSettings.requireGender = !0, 
            interactionFormSettings.requireEmail = !0, interactionFormSettings.identifier) {
              case "both":
                interactionFormSettings.requireEmail = interactionFormSettings.requirePhone = !0;
                break;

              case "email":
                interactionFormSettings.requireEmail = !0;
                break;

              case "phone":
                interactionFormSettings.requirePhone = !0;
                break;

              case "either":
                interactionFormSettings.askPhone = !0, interactionFormSettings.askEmail = !0;
            }
            var firstNameField, lastNameField, genderField;
            if ((interactionFormSettings.askGender || interactionFormSettings.requireGender) && (genderField = {
                key: "_gender",
                type: "select",
                templateOptions: {
                    type: "email",
                    label: "Title",
                    placeholder: "Please select a title",
                    options: [ {
                        name: "Mr",
                        value: "male"
                    }, {
                        name: "Ms / Mrs",
                        value: "female"
                    } ],
                    required: interactionFormSettings.requireGender,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                },
                validators: {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        return "male" == value || "female" == value;
                    }
                }
            }), (interactionFormSettings.askFirstName || interactionFormSettings.requireFirstName) && (firstNameField = {
                key: "_firstName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "First Name",
                    placeholder: "Please enter your first name",
                    required: interactionFormSettings.requireFirstName,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            }), (interactionFormSettings.askLastName || interactionFormSettings.requireLastName) && (lastNameField = {
                key: "_lastName",
                type: "input",
                templateOptions: {
                    type: "text",
                    label: "Last Name",
                    placeholder: "Please enter your last name",
                    required: interactionFormSettings.requireLastName,
                    onBlur: "to.focused=false",
                    onFocus: "to.focused=true"
                }
            }), genderField && firstNameField && lastNameField ? (genderField.className = "col-sm-2", 
            firstNameField.className = lastNameField.className = "col-sm-5", $scope.vm.modelFields.push({
                fieldGroup: [ genderField, firstNameField, lastNameField ],
                className: "row"
            })) : firstNameField && lastNameField && !genderField ? (firstNameField.className = lastNameField.className = "col-sm-6", 
            $scope.vm.modelFields.push({
                fieldGroup: [ firstNameField, lastNameField ],
                className: "row"
            })) : (genderField && $scope.vm.modelFields.push(genderField), firstNameField && $scope.vm.modelFields.push(firstNameField), 
            lastNameField && $scope.vm.modelFields.push(lastNameField)), interactionFormSettings.askEmail || interactionFormSettings.requireEmail) {
                var newField = {
                    key: "_email",
                    type: "input",
                    templateOptions: {
                        type: "email",
                        label: "Email Address",
                        placeholder: "Please enter a valid email address",
                        required: interactionFormSettings.requireEmail,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var value = $modelValue || $viewValue;
                            return validator.isEmail(value);
                        }
                    }
                };
                "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                    "templateOptions.required": function($viewValue, $modelValue, scope) {
                        return scope.model._phoneNumber && scope.model._phoneNumber.length ? !1 : !0;
                    }
                }), $scope.vm.modelFields.push(newField);
            }
            if (interactionFormSettings.askPhone || interactionFormSettings.requirePhone) {
                var newField = {
                    key: "_phoneNumber",
                    type: "input",
                    templateOptions: {
                        type: "tel",
                        label: "Contact Phone Number",
                        placeholder: "Please enter a contact phone number",
                        required: interactionFormSettings.requirePhone,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                };
                "either" == interactionFormSettings.identifier && (newField.expressionProperties = {
                    "templateOptions.required": function($viewValue, $modelValue, scope) {
                        return scope.model._email && scope.model._email.length ? !1 : !0;
                    }
                }), $scope.vm.modelFields.push(newField);
            }
            if (interactionFormSettings.askDOB || interactionFormSettings.requireDOB) {
                var newField = {
                    key: "_dob",
                    type: "dob-select",
                    templateOptions: {
                        label: "Date of birth",
                        placeholder: "Please provide your date of birth",
                        required: interactionFormSettings.requireDOB,
                        maxDate: new Date(),
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                };
                $scope.vm.modelFields.push(newField);
            }
            _.each($scope.model.fields, function(fieldDefinition) {
                addFieldDefinition($scope.vm.modelFields, fieldDefinition);
            }), $scope.model.paymentDetails || ($scope.model.paymentDetails = {});
            var paymentSettings = $scope.model.paymentDetails;
            if (paymentSettings.required || paymentSettings.allow) {
                var paymentWrapperFields = [], paymentCardFields = [];
                if (paymentSettings.required) console.log("Payment is set to required"), paymentWrapperFields.push({
                    templateUrl: "fluro-interaction-form/payment/payment-summary.html",
                    controller: function($scope, $parse) {
                        function calculateTotal() {
                            return console.log("calculate"), $scope.debugMode && console.log("Recalculate total"), 
                            $scope.calculatedTotal = requiredAmount, $scope.modifications = [], paymentSettings.modifiers && paymentSettings.modifiers.length ? (_.each(paymentSettings.modifiers, function(modifier) {
                                var parsedValue = $parse(modifier.expression)($scope);
                                if (parsedValue = Number(parsedValue), isNaN(parsedValue)) return void ($scope.debugMode && console.log("Payment modifier error", modifier.title, parsedValue));
                                var parsedCondition = !0;
                                if (modifier.condition && String(modifier.condition).length && (parsedCondition = $parse(modifier.condition)($scope)), 
                                !parsedCondition) return void ($scope.debugMode && console.log("inactive", modifier.title, modifier, $scope));
                                var operator = "", operatingValue = "$" + parseFloat(parsedValue / 100).toFixed(2);
                                switch (modifier.operation) {
                                  case "add":
                                    operator = "+", $scope.calculatedTotal = $scope.calculatedTotal + parsedValue;
                                    break;

                                  case "subtract":
                                    operator = "-", $scope.calculatedTotal = $scope.calculatedTotal - parsedValue;
                                    break;

                                  case "divide":
                                    operator = "÷", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal / parsedValue;
                                    break;

                                  case "multiply":
                                    operator = "×", operatingValue = parsedValue, $scope.calculatedTotal = $scope.calculatedTotal * parsedValue;
                                    break;

                                  case "set":
                                    $scope.calculatedTotal = parsedValue;
                                }
                                $scope.modifications.push({
                                    title: modifier.title,
                                    total: $scope.calculatedTotal,
                                    description: operator + " " + operatingValue,
                                    operation: modifier.operation
                                });
                            }), (!$scope.calculatedTotal || isNaN($scope.calculatedTotal) || $scope.calculatedTotal < 0) && ($scope.calculatedTotal = 0), 
                            void $timeout(function() {
                                $formScope.vm.model.calculatedTotal = $scope.calculatedTotal;
                            })) : void ($scope.debugMode && console.log("No payment modifiers set"));
                        }
                        $scope.paymentDetails = paymentSettings;
                        var requiredAmount = paymentSettings.amount;
                        $formScope.vm.model.calculatedTotal = requiredAmount, $scope.calculatedTotal = requiredAmount;
                        var watchString = "", modelVariables = _.chain(paymentSettings.modifiers).map(function(paymentModifier) {
                            var string = "(" + paymentModifier.expression + ") + (" + paymentModifier.condition + ")";
                            return string;
                        }).flatten().compact().uniq().value();
                        modelVariables.length && (watchString = modelVariables.join(" + ")), watchString.length ? ($scope.debugMode && console.log("Watching changes", watchString), 
                        $scope.$watch(watchString, calculateTotal)) : (console.log("No watch string provided"), 
                        $scope.calculatedTotal = requiredAmount, $scope.modifications = []);
                    }
                }); else {
                    var amountDescription = "Please enter an amount (" + String(paymentSettings.currency).toUpperCase() + ")", minimum = paymentSettings.minAmount, maximum = paymentSettings.maxAmount, paymentErrorMessage = (paymentSettings.amount, 
                    "Invalid amount");
                    minimum && (minimum = parseInt(minimum) / 100, paymentErrorMessage = "Amount must be a number at least " + $filter("currency")(minimum, "$"), 
                    amountDescription += "Enter at least " + $filter("currency")(minimum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                    maximum && (maximum = parseInt(maximum) / 100, paymentErrorMessage = "Amount must be a number less than " + $filter("currency")(maximum, "$"), 
                    amountDescription += "Enter up to " + $filter("currency")(maximum, "$") + " " + String(paymentSettings.currency).toUpperCase()), 
                    minimum && maximum && (amountDescription = "Enter a numeric amount between " + $filter("currency")(minimum) + " and  " + $filter("currency")(maximum) + " " + String(paymentSettings.currency).toUpperCase(), 
                    paymentErrorMessage = "Amount must be a number between " + $filter("currency")(minimum) + " and " + $filter("currency")(maximum));
                    var fieldConfig = {
                        key: "_paymentAmount",
                        type: "currency",
                        templateOptions: {
                            type: "text",
                            label: "Amount",
                            description: amountDescription,
                            placeholder: "0.00",
                            required: !0,
                            errorMessage: paymentErrorMessage,
                            min: minimum,
                            max: maximum,
                            onBlur: "to.focused=false",
                            onFocus: "to.focused=true"
                        },
                        data: {
                            customMaxLength: 8,
                            minimumAmount: minimum,
                            maximumAmount: maximum
                        }
                    };
                    minimum && (fieldConfig.defaultValue = minimum), paymentWrapperFields.push({
                        template: "<hr/><h3>Payment Details</h3>"
                    }), paymentWrapperFields.push(fieldConfig);
                }
                var defaultCardName, defaultCardNumber, defaultCardExpMonth, defaultCardExpYear, defaultCardCVN;
                $scope.debugMode && (defaultCardName = "John Citizen", defaultCardNumber = "4242424242424242", 
                defaultCardExpMonth = "05", defaultCardExpYear = "2020", defaultCardCVN = "123"), 
                paymentCardFields.push({
                    key: "_paymentCardName",
                    type: "input",
                    defaultValue: defaultCardName,
                    templateOptions: {
                        type: "text",
                        label: "Card Name",
                        placeholder: "Card Name",
                        required: paymentSettings.required,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    }
                }), paymentCardFields.push({
                    key: "_paymentCardNumber",
                    type: "input",
                    defaultValue: defaultCardNumber,
                    templateOptions: {
                        type: "text",
                        label: "Card Number",
                        placeholder: "Card Number",
                        required: paymentSettings.required,
                        onBlur: "to.focused=false",
                        onFocus: "to.focused=true"
                    },
                    validators: {
                        validInput: function($viewValue, $modelValue, scope) {
                            var luhnChk = function(a) {
                                return function(c) {
                                    if (!c) return !1;
                                    for (var v, l = c.length, b = 1, s = 0; l; ) v = parseInt(c.charAt(--l), 10), s += (b ^= 1) ? a[v] : v;
                                    return s && 0 === s % 10;
                                };
                            }([ 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 ]), value = $modelValue || $viewValue, valid = luhnChk(value);
                            return valid;
                        }
                    }
                }), paymentCardFields.push({
                    className: "row clearfix",
                    fieldGroup: [ {
                        key: "_paymentCardExpMonth",
                        className: "col-xs-6 col-sm-5",
                        type: "input",
                        defaultValue: defaultCardExpMonth,
                        templateOptions: {
                            type: "text",
                            label: "Expiry Month",
                            placeholder: "MM",
                            required: paymentSettings.required,
                            onBlur: "to.focused=false",
                            onFocus: "to.focused=true"
                        }
                    }, {
                        key: "_paymentCardExpYear",
                        className: "col-xs-6 col-sm-5",
                        type: "input",
                        defaultValue: defaultCardExpYear,
                        templateOptions: {
                            type: "text",
                            label: "Expiry Year",
                            placeholder: "YYYY",
                            required: paymentSettings.required,
                            onBlur: "to.focused=false",
                            onFocus: "to.focused=true"
                        }
                    }, {
                        key: "_paymentCardCVN",
                        className: "col-xs-4 col-sm-2",
                        type: "input",
                        defaultValue: defaultCardCVN,
                        templateOptions: {
                            type: "text",
                            label: "CVN",
                            placeholder: "CVN",
                            required: paymentSettings.required,
                            onBlur: "to.focused=false",
                            onFocus: "to.focused=true"
                        }
                    } ]
                });
                var cardDetailsField = {
                    className: "payment-details",
                    fieldGroup: paymentCardFields,
                    hideExpression: function($viewValue, $modelValue, scope) {
                        return 0 === $formScope.vm.model.calculatedTotal ? !0 : void 0;
                    }
                };
                if (paymentSettings.allowAlternativePayments && paymentSettings.paymentMethods && paymentSettings.paymentMethods.length) {
                    var methodSelection = {
                        className: "payment-method-select",
                        data: {
                            fields: [ cardDetailsField ],
                            settings: paymentSettings
                        },
                        controller: function($scope) {
                            $scope.settings = paymentSettings, $scope.methodOptions = _.map(paymentSettings.paymentMethods, function(method) {
                                return method;
                            }), $scope.methodOptions.unshift({
                                title: "Pay with Card",
                                key: "card"
                            }), $scope.model._paymentMethod || ($scope.model._paymentMethod = "card"), $scope.selected = {
                                method: $scope.methodOptions[0]
                            }, $scope.selectMethod = function(method) {
                                $scope.settings.showOptions = !1, $scope.selected.method = method, $scope.model._paymentMethod = method.key;
                            };
                        },
                        templateUrl: "fluro-interaction-form/payment/payment-method.html",
                        hideExpression: function($viewValue, $modelValue, scope) {
                            return 0 === $formScope.vm.model.calculatedTotal ? !0 : void 0;
                        }
                    };
                    paymentWrapperFields.push(methodSelection);
                } else paymentWrapperFields.push(cardDetailsField);
                $scope.vm.modelFields.push({
                    fieldGroup: paymentWrapperFields
                });
            }
            $scope.promises.length ? ($scope.promisesResolved = !1, $q.all($scope.promises).then(function() {
                $scope.promisesResolved = !0;
            })) : $scope.promisesResolved = !0;
        }
    }), $scope.$watch("vm.modelFields", function(fields) {
        $scope.errorList = getAllErrorFields(fields);
    }, !0);
}), app.directive("postForm", function($compile) {
    return {
        restrict: "E",
        scope: {
            model: "=ngModel",
            host: "=hostId",
            reply: "=?reply",
            thread: "=?thread",
            userStore: "=?user",
            vm: "=?config",
            debugMode: "=?debugMode",
            callback: "=?callback"
        },
        transclude: !0,
        controller: "PostFormController",
        templateUrl: "fluro-interaction-form/fluro-web-form.html",
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $scope.transcludedContent = clone;
            });
        }
    };
}), app.directive("recaptchaRender", function($window) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs, $ctrl) {
            function activateRecaptcha(recaptcha) {
                console.log("Activate recaptcha!!"), cancelWatch && cancelWatch(), recaptcha && ($scope.vm.recaptchaID = recaptcha.render(element, {
                    sitekey: "6LelOyUTAAAAADSACojokFPhb9AIzvrbGXyd-33z"
                }));
            }
            if ($scope.model.data && $scope.model.data.recaptcha) {
                var cancelWatch, element = $element[0];
                window.grecaptcha ? activateRecaptcha(window.grecaptcha) : cancelWatch = $scope.$watch(function() {
                    return window.grecaptcha;
                }, activateRecaptcha);
            }
        }
    };
}), app.controller("PostFormController", function($scope, $rootScope, $q, $http, Fluro, FluroAccess, $parse, $filter, formlyValidationMessages, FluroContent, FluroContentRetrieval, FluroValidate, FluroInteraction) {
    function resetCaptcha() {
        var recaptchaID = $scope.vm.recaptchaID;
        console.log("Reset Captcha", recaptchaID), window.grecaptcha && recaptchaID && window.grecaptcha.reset(recaptchaID);
    }
    function getAllErrorFields(array) {
        return _.chain(array).map(function(field) {
            if (field.fieldGroup && field.fieldGroup.length) return getAllErrorFields(field.fieldGroup);
            if (field.data && (field.data.fields && field.data.fields.length || field.data.dataFields && field.data.dataFields || field.data.replicatedFields && field.data.replicatedFields)) {
                var combined = [];
                return combined = combined.concat(field.data.fields, field.data.dataFields, field.data.replicatedFields), 
                combined = _.compact(combined), getAllErrorFields(combined);
            }
            return field;
        }).flatten().value();
    }
    function submitPost() {
        function submissionSuccess(res) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {
                data: {}
            }, $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
            $scope.response = res, $scope.thread && $scope.thread.push(res), $scope.vm.state = "complete";
        }
        function submissionFail(res) {
            return $scope.vm.state = "error", res.data ? res.data.error ? res.data.error.message ? $scope.processErrorMessages = [ res.error.message ] : $scope.processErrorMessages = [ res.error ] : res.data.errors ? $scope.processErrorMessages = _.map(res.data.errors, function(error) {
                return error.message;
            }) : _.isArray(res.data) ? $scope.processErrorMessages = res.data : void ($scope.processErrorMessages = [ res.data ]) : $scope.processErrorMessages = [ "Error: " + res ];
        }
        $scope.vm.state = "sending";
        var submissionKey = $scope.model.definitionName, submissionModel = {
            data: angular.copy($scope.vm.model)
        }, hostID = $scope.host;
        if ($scope.reply && (submissionModel.reply = $scope.reply), "undefined" != typeof $scope.vm.recaptchaID) {
            var response = window.grecaptcha.getResponse($scope.vm.recaptchaID);
            submissionModel["g-recaptcha-response"] = response;
        }
        var request;
        $scope.userStore ? $scope.userStore.config().then(function(config) {
            var postURL = Fluro.apiURL + "/post/" + hostID + "/" + submissionKey;
            request = $http.post(postURL, submissionModel, config), request.then(function(res) {
                return submissionSuccess(res.data);
            }, function(res) {
                return submissionFail(res.data);
            });
        }) : (request = FluroContent.endpoint("post/" + hostID + "/" + submissionKey).save(submissionModel).$promise, 
        request.then(submissionSuccess, submissionFail));
    }
    $scope.thread || ($scope.thread = []), $scope.vm || ($scope.vm = {}), $scope.promisesResolved = !0, 
    $scope.correctPermissions = !0, $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
    $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.readyToSubmit = !1, 
    $scope.$watch("vm.modelForm.$invalid + vm.modelForm.$error", function() {
        var interactionForm = $scope.vm.modelForm;
        if (!interactionForm) return $scope.readyToSubmit = !1;
        if (interactionForm.$invalid) return $scope.readyToSubmit = !1;
        if (interactionForm.$error) {
            if (interactionForm.$error.required && interactionForm.$error.required.length) return $scope.readyToSubmit = !1;
            if (interactionForm.$error.validInput && interactionForm.$error.validInput.length) return $scope.readyToSubmit = !1;
        }
        $scope.readyToSubmit = !0;
    }, !0), formlyValidationMessages.addStringMessage("required", "This field is required"), 
    formlyValidationMessages.messages.validInput = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid value";
    }, formlyValidationMessages.messages.date = function($viewValue, $modelValue, scope) {
        return scope.to.label + " is not a valid date";
    }, $scope.reset = function() {
        $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
        $scope.vm.modelForm.$setPristine(), $scope.vm.options.resetModel(), resetCaptcha(), 
        $scope.response = null, $scope.vm.state = "ready", console.log("Broadcast reset"), 
        $scope.$broadcast("form-reset");
    }, $scope.$watch("model", function(newData, oldData) {
        function addFieldDefinition(array, fieldDefinition) {
            if (!fieldDefinition.params || !fieldDefinition.params.disableWebform) {
                var newField = {};
                newField.key = fieldDefinition.key, fieldDefinition.className && (newField.className = fieldDefinition.className);
                var templateOptions = {};
                switch (templateOptions.type = "text", templateOptions.label = fieldDefinition.title, 
                templateOptions.description = fieldDefinition.description, templateOptions.params = fieldDefinition.params, 
                fieldDefinition.errorMessage && (templateOptions.errorMessage = fieldDefinition.errorMessage), 
                templateOptions.definition = fieldDefinition, fieldDefinition.placeholder && fieldDefinition.placeholder.length ? templateOptions.placeholder = fieldDefinition.placeholder : fieldDefinition.description && fieldDefinition.description.length ? templateOptions.placeholder = fieldDefinition.description : templateOptions.placeholder = fieldDefinition.title, 
                templateOptions.required = fieldDefinition.minimum > 0, templateOptions.onBlur = "to.focused=false", 
                templateOptions.onFocus = "to.focused=true", fieldDefinition.directive) {
                  case "reference-select":
                  case "value-select":
                    newField.type = "button-select";
                    break;

                  case "select":
                    newField.type = "select";
                    break;

                  case "wysiwyg":
                    newField.type = "textarea";
                    break;

                  default:
                    newField.type = fieldDefinition.directive;
                }
                switch (fieldDefinition.type) {
                  case "reference":
                    if (fieldDefinition.allowedReferences && fieldDefinition.allowedReferences.length) templateOptions.options = _.map(fieldDefinition.allowedReferences, function(ref) {
                        return {
                            name: ref.title,
                            value: ref._id
                        };
                    }); else if (templateOptions.options = [], fieldDefinition.sourceQuery) {
                        var queryId = fieldDefinition.sourceQuery;
                        queryId._id && (queryId = queryId._id);
                        var options = {};
                        fieldDefinition.queryTemplate && (options.template = fieldDefinition.queryTemplate, 
                        options.template._id && (options.template = options.template._id));
                        var promise = FluroContentRetrieval.getQuery(queryId, options);
                        promise.then(function(res) {
                            templateOptions.options = _.map(res, function(ref) {
                                return {
                                    name: ref.title,
                                    value: ref._id
                                };
                            });
                        });
                    } else "embedded" != fieldDefinition.directive && fieldDefinition.params.restrictType && fieldDefinition.params.restrictType.length && FluroContent.resource(fieldDefinition.params.restrictType).query().$promise.then(function(res) {
                        templateOptions.options = _.map(res, function(ref) {
                            return {
                                name: ref.title,
                                value: ref._id
                            };
                        });
                    });
                    break;

                  default:
                    fieldDefinition.options && fieldDefinition.options.length ? templateOptions.options = fieldDefinition.options : templateOptions.options = _.map(fieldDefinition.allowedValues, function(val) {
                        return {
                            name: val,
                            value: val
                        };
                    });
                }
                if (fieldDefinition.attributes && _.keys(fieldDefinition.attributes).length && (newField.ngModelAttrs = _.reduce(fieldDefinition.attributes, function(results, attr, key) {
                    var customKey = "customAttr" + key;
                    return results[customKey] = {
                        attribute: key
                    }, templateOptions[customKey] = attr, results;
                }, {})), "custom" != fieldDefinition.directive) switch (fieldDefinition.type) {
                  case "boolean":
                    fieldDefinition.params && fieldDefinition.params.storeCopy ? newField.type = "terms" : newField.type = "checkbox";
                    break;

                  case "number":
                  case "float":
                  case "integer":
                  case "decimal":
                    templateOptions.type = "input", newField.ngModelAttrs || (newField.ngModelAttrs = {}), 
                    "integer" == fieldDefinition.type && (templateOptions.type = "number", templateOptions.baseDefaultValue = 0, 
                    newField.ngModelAttrs.customAttrpattern = {
                        attribute: "pattern"
                    }, newField.ngModelAttrs.customAttrinputmode = {
                        attribute: "inputmode"
                    }, templateOptions.customAttrpattern = "[0-9]*", templateOptions.customAttrinputmode = "numeric", 
                    fieldDefinition.params && (0 !== parseInt(fieldDefinition.params.maxValue) && (templateOptions.max = fieldDefinition.params.maxValue), 
                    0 !== parseInt(fieldDefinition.params.minValue) ? templateOptions.min = fieldDefinition.params.minValue : templateOptions.min = 0));
                }
                if (1 == fieldDefinition.maximum ? "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length && ("search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0] : templateOptions.baseDefaultValue = fieldDefinition.defaultReferences[0]._id) : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = Number(fieldDefinition.defaultValues[0]) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues[0]) : "reference" == fieldDefinition.type && "embedded" != fieldDefinition.directive ? fieldDefinition.defaultReferences && fieldDefinition.defaultReferences.length ? "search-select" == fieldDefinition.directive ? templateOptions.baseDefaultValue = fieldDefinition.defaultReferences : templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultReferences, function(ref) {
                    return ref._id;
                }) : templateOptions.baseDefaultValue = [] : fieldDefinition.defaultValues && fieldDefinition.defaultValues.length && ("number" == templateOptions.type ? templateOptions.baseDefaultValue = _.map(fieldDefinition.defaultValues, function(val) {
                    return Number(val);
                }) : templateOptions.baseDefaultValue = fieldDefinition.defaultValues), newField.templateOptions = templateOptions, 
                newField.validators = {
                    validInput: function($viewValue, $modelValue, scope) {
                        var value = $modelValue || $viewValue;
                        if (!value) return !0;
                        var valid = FluroValidate.validate(value, fieldDefinition);
                        return valid;
                    }
                }, "embedded" == fieldDefinition.directive) {
                    if (newField.type = "embedded", 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {
                        data: {}
                    }; else {
                        var askCount = 0;
                        fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                        fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                        var initialArray = [];
                        askCount && _.times(askCount, function() {
                            initialArray.push({});
                        }), templateOptions.baseDefaultValue = initialArray;
                    }
                    newField.data = {
                        fields: [],
                        dataFields: [],
                        replicatedFields: []
                    };
                    var fieldContainer = newField.data.fields, dataFieldContainer = newField.data.dataFields;
                    fieldDefinition.fields && fieldDefinition.fields.length && _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                    var promise = FluroContent.endpoint("defined/" + fieldDefinition.params.restrictType).get().$promise;
                    promise.then(function(embeddedDefinition) {
                        if (embeddedDefinition && embeddedDefinition.fields && embeddedDefinition.fields.length) {
                            var childFields = embeddedDefinition.fields;
                            fieldDefinition.params.excludeKeys && fieldDefinition.params.excludeKeys.length && (childFields = _.reject(childFields, function(f) {
                                return _.includes(fieldDefinition.params.excludeKeys, f.key);
                            })), _.each(childFields, function(sub) {
                                addFieldDefinition(dataFieldContainer, sub);
                            });
                        }
                    }), $scope.promises.push(promise);
                }
                if ("group" == fieldDefinition.type && fieldDefinition.fields && fieldDefinition.fields.length || fieldDefinition.asObject) {
                    var fieldContainer;
                    if (fieldDefinition.asObject) {
                        if (newField.type = "nested", fieldDefinition.key && 1 == fieldDefinition.maximum && 1 == fieldDefinition.minimum) templateOptions.baseDefaultValue = {}; else {
                            var askCount = 0;
                            fieldDefinition.askCount && (askCount = fieldDefinition.askCount), fieldDefinition.minimum && askCount < fieldDefinition.minimum && (askCount = fieldDefinition.minimum), 
                            fieldDefinition.maximum && askCount > fieldDefinition.maximum && (askCount = fieldDefinition.maximum);
                            var initialArray = [];
                            askCount && _.times(askCount, function() {
                                initialArray.push({});
                            }), templateOptions.baseDefaultValue = initialArray;
                        }
                        newField.data = {
                            fields: [],
                            replicatedFields: []
                        }, fieldContainer = newField.data.fields;
                    } else newField = {
                        fieldGroup: [],
                        className: fieldDefinition.className
                    }, fieldContainer = newField.fieldGroup;
                    _.each(fieldDefinition.fields, function(sub) {
                        addFieldDefinition(fieldContainer, sub);
                    });
                }
                if (fieldDefinition.expressions && _.keys(fieldDefinition.expressions).length) {
                    fieldDefinition.hideExpression && fieldDefinition.hideExpression.length && (fieldDefinition.expressions.hide = fieldDefinition.hideExpression);
                    var allExpressions = _.values(fieldDefinition.expressions).join("+");
                    newField.watcher = {
                        expression: function(field, scope) {
                            return $parse(allExpressions)(scope);
                        },
                        listener: function(field, newValue, oldValue, scope, stopWatching) {
                            scope.interaction || (scope.interaction = $scope.vm.model), _.each(fieldDefinition.expressions, function(expression, key) {
                                var retrievedValue = $parse(expression)(scope), fieldKey = field.key;
                                switch (key) {
                                  case "defaultValue":
                                    if (!field.formControl || !field.formControl.$dirty) return scope.model[fieldKey] = retrievedValue;
                                    break;

                                  case "value":
                                    return scope.model[fieldKey] = retrievedValue;

                                  case "required":
                                    return field.templateOptions.required = retrievedValue;

                                  case "hide":
                                    return field.hide = retrievedValue;
                                }
                            });
                        }
                    };
                }
                fieldDefinition.hideExpression && (newField.hideExpression = fieldDefinition.hideExpression), 
                newField.fieldGroup || (newField.defaultValue = angular.copy(templateOptions.baseDefaultValue)), 
                "pathlink" != newField.type && array.push(newField);
            }
        }
        if ($scope.model && "post" == $scope.model.parentType) {
            $scope.vm.defaultModel ? $scope.vm.model = angular.copy($scope.vm.defaultModel) : $scope.vm.model = {}, 
            $scope.vm.modelFields = [], $scope.vm.state = "ready", $scope.vm.onSubmit = submitPost, 
            $scope.promises = [], $scope.submitLabel = "Submit", $scope.model && $scope.model.data && $scope.model.data.submitLabel && $scope.model.data.submitLabel.length && ($scope.submitLabel = $scope.model.data.submitLabel);
            var interactionFormSettings = $scope.model.data;
            interactionFormSettings || (interactionFormSettings = {}), _.each($scope.model.fields, function(fieldDefinition) {
                addFieldDefinition($scope.vm.modelFields, fieldDefinition);
            });
        }
    }), $scope.$watch("vm.modelFields", function(fields) {
        $scope.errorList = getAllErrorFields(fields);
    }, !0);
}), app.directive("postThread", function(FluroContent) {
    return {
        restrict: "E",
        transclude: !0,
        scope: {
            definitionName: "=?type",
            host: "=?hostId",
            thread: "=?thread"
        },
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        },
        controller: function($scope, $filter) {
            $scope.outer = $scope.$parent, $scope.thread || ($scope.thread = []), $scope.$watch("host + definitionName", function() {
                function postsLoaded(res) {
                    var allPosts = res;
                    $scope.thread = _.chain(res).map(function(post) {
                        return post.thread = _.filter(allPosts, function(sub) {
                            return sub.reply == post._id;
                        }), post.reply ? void 0 : post;
                    }).compact().value();
                }
                function postsError(res) {
                    $scope.thread = [];
                }
                var hostID = $scope.host, definitionName = $scope.definitionName;
                if (hostID && definitionName) {
                    var request = FluroContent.endpoint("post/" + hostID + "/" + definitionName, !0, !0).query().$promise;
                    request.then(postsLoaded, postsError);
                }
            });
        }
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "nested",
        templateUrl: "fluro-interaction-form/nested/fluro-nested.html",
        controller: "FluroInteractionNestedController"
    });
}), app.controller("FluroInteractionNestedController", function($scope) {
    function resetDefaultValue() {
        var defaultValue = angular.copy($scope.to.baseDefaultValue);
        $scope.model || console.log("NO RESET Reset Model Values", $scope.options.key, defaultValue), 
        $scope.model[$scope.options.key] = defaultValue;
    }
    var def = $scope.to.definition, minimum = def.minimum, maximum = def.maximum;
    $scope.$watch("model[options.key]", function(model) {
        model || resetDefaultValue();
    }), $scope.$on("form-reset", resetDefaultValue), $scope.addAnother = function() {
        console.log("Add another"), $scope.model[$scope.options.key].push({});
    }, $scope.canRemove = function() {
        return minimum ? $scope.model[$scope.options.key].length > minimum ? !0 : void 0 : !0;
    }, $scope.canAdd = function() {
        return maximum ? $scope.model[$scope.options.key].length < maximum ? !0 : void 0 : !0;
    }, $scope.copyFields = function() {
        var copiedFields = angular.copy($scope.options.data.fields);
        return $scope.options.data.replicatedFields.push(copiedFields), copiedFields;
    }, $scope.copyDataFields = function() {
        var copiedFields = angular.copy($scope.options.data.dataFields);
        return $scope.options.data.replicatedFields.push(copiedFields), copiedFields;
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "search-select",
        templateUrl: "fluro-interaction-form/search-select/fluro-search-select.html",
        controller: "FluroSearchSelectController",
        wrapper: [ "bootstrapLabel", "bootstrapHasError" ]
    });
}), app.controller("FluroSearchSelectController", function($scope, $http, Fluro, $filter, FluroValidate) {
    function setModel() {
        $scope.multiple ? $scope.model[opts.key] = angular.copy($scope.selection.values) : $scope.model[opts.key] = angular.copy($scope.selection.value), 
        $scope.fc && $scope.fc.$setTouched(), checkValidity();
    }
    function checkValidity() {
        var validRequired, validInput = FluroValidate.validate($scope.model[$scope.options.key], definition);
        $scope.multiple ? $scope.to.required && (validRequired = _.isArray($scope.model[opts.key]) && $scope.model[opts.key].length > 0) : $scope.to.required && $scope.model[opts.key] && (validRequired = !0), 
        $scope.fc && ($scope.fc.$setValidity("required", validRequired), $scope.fc.$setValidity("validInput", validInput));
    }
    $scope.search = {}, $scope.proposed = {};
    var opts = ($scope.to, $scope.options);
    $scope.selection = {};
    var definition = $scope.to.definition;
    definition.params || (definition.params = {});
    var restrictType = definition.params.restrictType, searchLimit = definition.params.searchLimit;
    searchLimit || (searchLimit = 10);
    var minimum = definition.minimum, maximum = definition.maximum;
    if (minimum || (minimum = 0), maximum || (maximim = 0), $scope.multiple = 1 != maximum, 
    $scope.multiple ? $scope.model[opts.key] && _.isArray($scope.model[opts.key]) && ($scope.selection.values = angular.copy($scope.model[opts.key])) : $scope.model[opts.key] && ($scope.selection.value = $scope.model[opts.key]), 
    $scope.canAddMore = function() {
        return maximum ? $scope.multiple ? $scope.selection.values.length < maximum : $scope.selection.value ? void 0 : !0 : !0;
    }, $scope.contains = function(value) {
        return $scope.multiple ? _.includes($scope.selection.values, value) : $scope.selection.value == value;
    }, $scope.$watch("model", function(newModelValue, oldModelValue) {
        if (newModelValue != oldModelValue) {
            var modelValue;
            _.keys(newModelValue).length && (modelValue = newModelValue[opts.key], $scope.multiple ? modelValue && _.isArray(modelValue) ? $scope.selection.values = angular.copy(modelValue) : $scope.selection.values = [] : $scope.selection.value = angular.copy(modelValue));
        }
    }, !0), opts.expressionProperties && opts.expressionProperties["templateOptions.required"] && $scope.$watch(function() {
        return $scope.to.required;
    }, function(newValue) {
        checkValidity();
    }), $scope.to.required) var unwatchFormControl = $scope.$watch("fc", function(newValue) {
        newValue && (checkValidity(), unwatchFormControl());
    });
    $scope.select = function(value) {
        if ($scope.multiple) {
            if (!$scope.canAddMore()) return;
            $scope.selection.values.push(value);
        } else $scope.selection.value = value;
        $scope.proposed = {}, setModel();
    }, $scope.retrieveReferenceOptions = function(val) {
        var searchUrl = Fluro.apiURL + "/content";
        return restrictType && (searchUrl += "/" + restrictType), searchUrl += "/search", 
        $http.get(searchUrl + "/" + val, {
            ignoreLoadingBar: !0,
            params: {
                limit: searchLimit
            }
        }).then(function(response) {
            var results = response.data;
            return _.reduce(results, function(filtered, item) {
                var exists = _.some($scope.selection.values, {
                    _id: item._id
                });
                return exists || filtered.push(item), filtered;
            }, []);
        });
    }, $scope.getValueLabel = function(value) {
        if (definition.options && definition.options.length) {
            var match = _.find(definition.options, {
                value: value
            });
            if (match && match.name) return match.name;
        }
        return value;
    }, $scope.retrieveValueOptions = function(val) {
        if (definition.options && definition.options.length) {
            var options = _.reduce(definition.options, function(results, item) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, item.value) : $scope.selection.value == item.value, 
                exists || results.push({
                    name: item.name,
                    value: item.value
                }), results;
            }, []);
            return $filter("filter")(options, val);
        }
        if (definition.allowedValues && definition.allowedValues.length) {
            var options = _.reduce(definition.allowedValues, function(results, allowedValue) {
                var exists;
                return exists = $scope.multiple ? _.includes($scope.selection.values, allowedValue) : $scope.selection.value == allowedValue, 
                exists || results.push({
                    name: allowedValue,
                    value: allowedValue
                }), results;
            }, []);
            return console.log("Options", options), $filter("filter")(options, val);
        }
    }, $scope.deselect = function(value) {
        $scope.multiple ? _.pull($scope.selection.values, value) : delete $scope.selection.value, 
        setModel();
    }, $scope.toggle = function(reference) {
        $scope.contains(reference) ? $scope.deselect(reference) : $scope.select(reference);
    };
}), app.run(function(formlyConfig, $templateCache) {
    formlyConfig.setType({
        name: "value",
        templateUrl: "fluro-interaction-form/value/value.html",
        wrapper: [ "bootstrapHasError" ]
    });
}), app.service("NotificationService", function($timeout) {
    var controller = {
        messages: []
    };
    return controller.lastMessage = function() {
        return _.last(controller.messages);
    }, controller.message = function(string, style, duration) {
        style || (style = "info"), duration || (duration = 3e3);
        var message = {
            text: string,
            style: style,
            duration: duration
        };
        controller.messages.push(message), $timeout(function() {
            _.pull(controller.messages, message);
        }, message.duration);
    }, controller;
}), app.directive("preloadImage", function() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            scope.aspect = angular.isDefined(attrs.aspect) ? scope.$parent.$eval(attrs.aspect) : 0, 
            scope.aspect ? element.wrap('<div class="preload-image-outer aspect-ratio" style="padding-bottom:' + scope.aspect + '%"></div>') : element.wrap('<div class="preload-image-outer"></div>');
            var preloader = angular.element('<span class="image-preloader"><i class="fa fa-spinner fa-spin"/></span>');
            element.on("load", function() {
                element.removeClass("preload-hide"), element.addClass("preload-show"), preloader.remove();
            }), element.on("error", function(err) {
                element.removeClass("preload-hide"), element.addClass("preload-show"), preloader.remove();
            }), attrs.ngSrc && attrs.ngSrc.length && (element.addClass("preload-hide"), element.parent().append(preloader));
        }
    };
}), app.directive("fluroPreloader", function() {
    return {
        restrict: "E",
        replace: !0,
        scope: {},
        templateUrl: "fluro-preloader/fluro-preloader.html",
        controller: "FluroPreloaderController",
        link: function(scope, element, attrs) {}
    };
}), app.controller("FluroPreloaderController", function($scope, $state, $rootScope, $timeout) {
    function hidePreloader(event, toState, toParams, fromState, fromParams, error) {
        preloadTimer && ($timeout.cancel(preloadTimer), preloadTimer = null), "loading" == $scope.preloader["class"] && $timeout(function() {
            $scope.preloader["class"] = "loaded";
        }, 600);
    }
    var preloadTimer;
    $scope.preloader = {
        "class": "reset"
    }, $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams, error) {
        $scope.preloader["class"] = "reset", preloadTimer = $timeout(function() {
            $scope.preloader["class"] = "loading";
        });
    }), $rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {
        preloadTimer && ($timeout.cancel(preloadTimer), preloadTimer = null), $scope.preloader["class"] = "reset";
    }), $rootScope.$on("$preloaderHide", hidePreloader), $rootScope.$on("$stateChangeSuccess", hidePreloader);
}), app.controller("PurchaseModalController", function($scope, fullProduct, existingCards, PurchaseService) {
    function purchaseSuccess(res) {
        $scope.settings.state = "purchased", console.log("Purchase Success!", res);
    }
    function purchaseFailed(res) {
        $scope.settings.state = "error", $scope.settings.errorMessage = res.data, console.log("Purchase failed!", res);
    }
    $scope.settings = {
        state: "ready"
    }, $scope.product = fullProduct, $scope.methods = existingCards;
    var paymentIntegration = PurchaseService.applicationPaymentGateway();
    paymentIntegration && (paymentIntegration.publicDetails && paymentIntegration.publicDetails.sandbox ? $scope.card = {
        number: "4242424242424242",
        name: "John Smith",
        exp_month: "02",
        exp_year: "2019",
        saveCard: !0
    } : $scope.card = {
        number: "",
        name: "",
        exp_month: "",
        exp_year: "",
        saveCard: !0
    }), $scope.purchaseWithCard = function(productID, cardDetails) {
        $scope.settings.state = "processing";
        var request = PurchaseService.purchase(productID, cardDetails);
        request.then(purchaseSuccess, purchaseFailed);
    }, $scope.purchaseWithMethod = function(productID, method) {
        $scope.settings.state = "processing";
        var request = PurchaseService.purchase(productID, null, method);
        request.then(purchaseSuccess, purchaseFailed);
    }, $scope.purchaseFree = function(productID) {
        $scope.settings.state = "processing";
        var request = PurchaseService.purchase(productID);
        request.then(purchaseSuccess, purchaseFailed);
    };
}), app.service("PurchaseService", function($rootScope, $q, Fluro, FluroContent, $uibModal, $http) {
    var controller = {
        purchases: []
    };
    return controller.applicationPaymentGateway = function() {
        return _.get($rootScope, "applicationData.paymentGateway");
    }, controller.retrievePaymentMethods = function(integrationID) {
        var deferred = $q.defer();
        return integrationID && integrationID.length ? FluroContent.endpoint("payment/method/" + integrationID).query({}).$promise.then(deferred.resolve, deferred.reject) : deferred.reject("No payment gateway has been specified for this application"), 
        deferred.promise;
    }, controller.refreshPurchases = function() {
        function purchaseReloadSuccess(res) {
            controller.purchases = res.data, $rootScope.$broadcast("purchases.refreshed");
        }
        function purchaseReloadFail(err) {
            console.log("Error reloading purchases", err), controller.purchases = [];
        }
        if (!$rootScope.user) return controller.purchases = [];
        var accountID = $rootScope.user.account;
        accountID._id && (accountID = accountID._id);
        var promise = $http.get(Fluro.apiURL + "/my/purchases?simple=true&account=" + accountID);
        return promise.then(purchaseReloadSuccess, purchaseReloadFail), promise;
    }, controller.hasPurchased = function(productID) {
        return _.some(controller.purchases, {
            product: productID
        });
    }, controller.hasPurchasedAny = function(productIDs) {
        return _.some(controller.purchases, function(purchase) {
            return _.includes(productIDs, purchase.product);
        });
    }, controller.createPaymentMethod = function(card) {
        function createMethod(paymentMethod) {
            FluroContent.endpoint("payment/method/" + paymentGateway._id).save(paymentMethod).$promise.then(createMethodSuccess, createMethodFailed);
        }
        function createMethodFailed(err) {
            console.log("Error creating payment method", paymentGateway, err), deferred.reject(err);
        }
        function createMethodSuccess(res) {
            deferred.resolve(res);
        }
        var deferred = $q.defer(), paymentGateway = controller.applicationPaymentGateway();
        switch (paymentGateway && paymentGateway._id || createMethodFailed({
            data: "No payment gateway has been configured for this application"
        }), paymentGateway.module) {
          case "stripe":
            window.Stripe ? (paymentGateway.publicDetails && (paymentGateway.publicDetails.sandbox ? Stripe.setPublishableKey(paymentGateway.publicDetails.testPublicKey) : Stripe.setPublishableKey(paymentGateway.publicDetails.livePublicKey)), 
            Stripe.card.createToken(card, function(status, response) {
                if (response.error) createMethodFailed(response); else {
                    var token = response.id;
                    createMethod({
                        source: token
                    });
                }
            })) : (console.log("STRIPE.JS HAS NOT BEEN INCLUDED"), createMethodFailed({
                data: "stripe.js must be included in index.html"
            }));
            break;

          default:
            createMethodFailed({
                data: paymentGateway.module + " is not a valid payment integration"
            });
        }
        return deferred.promise;
    }, controller.removePaymentMethod = function(methodID) {
        return FluroContent.endpoint("payment/method/" + methodID)["delete"]().$promise.then(paymentMethodDeleted, paymentMethodError);
    }, controller.replacePaymentMethod = function(purchase, method) {
        function replaceMethodSuccess(res) {
            purchase.method = method;
        }
        function replaceMethodFailed(err) {
            console.log("Card Replace failed", err);
        }
        var methodID = method._id, purchaseID = purchase._id, request = FluroContent.endpoint("purchase/" + purchaseID + "/method/" + methodID).update({}).$promise;
        return request.then(replaceMethodSuccess, replaceMethodFailed), request;
    }, controller.cancelSubscription = function(purchase) {
        console.log("Cancelling", purchase);
        var promise = FluroContent.endpoint("purchase/" + purchase._id + "/cancel").update({}).$promise;
        return promise.then(controller.refreshPurchases), promise;
    }, controller.collectFreeProducts = function(productIDs, forceNewPurchase) {
        var paymentGateway = controller.applicationPaymentGateway(), requests = _.map(productIDs, function(productID) {
            var purchaseDetails = {};
            return purchaseDetails.product = productID, paymentGateway && paymentGateway._id && (purchaseDetails.integration = paymentGateway._id, 
            purchaseDetails.forceNewPurchase = forceNewPurchase), FluroContent.endpoint("payment/purchase").save(purchaseDetails).$promise;
        }), request = $q.all(requests);
        return request.then(function() {
            controller.refreshPurchases();
        }), request;
    }, controller.purchase = function(productID, card, method) {
        function purchaseComplete(res) {
            controller.refreshPurchases(), deferred.resolve(res);
        }
        function purchaseFailed(err) {
            console.log("Purchase Error", err), deferred.reject(err);
        }
        var deferred = $q.defer(), paymentGateway = controller.applicationPaymentGateway(), purchaseDetails = {};
        if (purchaseDetails.product = productID, paymentGateway && paymentGateway._id && (purchaseDetails.integration = paymentGateway._id), 
        method) {
            purchaseDetails.payment = {
                method: method._id
            };
            var request = FluroContent.endpoint("payment/purchase").save(purchaseDetails).$promise;
            return request.then(purchaseComplete, purchaseFailed), request;
        }
        if (card) switch (paymentGateway && paymentGateway._id || deferred.reject({
            data: "A payment gateway integration has not been configured for this application"
        }), card.saveCard && (purchaseDetails.saveCard = !0), paymentGateway.module) {
          case "stripe":
            if (window.Stripe) {
                paymentGateway.publicDetails && (paymentGateway.publicDetails.sandbox ? Stripe.setPublishableKey(paymentGateway.publicDetails.testPublicKey) : Stripe.setPublishableKey(paymentGateway.publicDetails.livePublicKey));
                var stripeCardParameters = {
                    number: card.number,
                    name: card.name,
                    exp_month: card.exp_month,
                    exp_year: card.exp_year
                };
                Stripe.card.createToken(stripeCardParameters, function(status, response) {
                    if (response.error) return deferred.reject({
                        data: response.error
                    });
                    var token = response.id;
                    purchaseDetails.payment = {
                        source: token
                    }, FluroContent.endpoint("payment/purchase").save(purchaseDetails).$promise.then(purchaseComplete, purchaseFailed);
                });
            } else console.log("STRIPE.JS HAS NOT BEEN INCLUDED"), deferred.reject({
                data: "stripe.js must be included in index.html"
            });
            break;

          default:
            console.log(paymentGateway.module + " is not a valid payment gateway"), deferred.reject(paymentGateway.module + " is not a valid payment gateway");
        } else FluroContent.endpoint("payment/purchase").save(purchaseDetails).$promise.then(purchaseComplete, purchaseFailed);
        return deferred.promise;
    }, controller.modal = function(productID, productDefinition) {
        if (!$rootScope.user) return console.log("Cant open");
        if (!productDefinition) return console.log("Please specify the type of product to open modal");
        productID._id && (productID = productID._id), console.log("Get product", productID);
        var modalInstance = $uibModal.open({
            backdrop: "static",
            templateUrl: "fluro-purchase-service/payment-modal.html",
            controller: "PurchaseModalController",
            resolve: {
                fullProduct: function(FluroContent) {
                    return FluroContent.resource(productDefinition + "/" + productID).get().$promise;
                },
                existingCards: function($q, FluroContent) {
                    var deferred = $q.defer(), paymentGateway = controller.applicationPaymentGateway();
                    return paymentGateway ? (paymentGateway._id && (paymentGateway = paymentGateway._id), 
                    controller.retrievePaymentMethods(paymentGateway)) : (deferred.resolve([]), deferred.promise);
                }
            }
        });
        return modalInstance;
    }, controller;
}), app.directive("scrollActive", function($compile, $timeout, $window, FluroScrollService) {
    return {
        restrict: "A",
        link: function($scope, $element, $attrs) {
            function setScrollContext(context) {
                currentContext != context && (currentContext = context, $timeout(function() {
                    switch (context) {
                      case "active":
                        $element.removeClass("scroll-after"), $element.removeClass("scroll-before"), $element.addClass("scroll-active"), 
                        $scope.scrollActive = !0, $scope.scrollBefore = !1, $scope.scrollAfter = !1, onActive && onActive();
                        break;

                      case "before":
                        $element.removeClass("scroll-after"), $element.addClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !0, $scope.scrollAfter = !1, onBefore && onBefore();
                        break;

                      case "after":
                        $element.addClass("scroll-after"), $element.removeClass("scroll-before"), $element.removeClass("scroll-active"), 
                        $scope.scrollActive = !1, $scope.scrollBefore = !1, $scope.scrollAfter = !0, onAfter && onAfter();
                    }
                }));
            }
            function updateParentScroll() {
                var viewportHeight = (parent.scrollTop(), parent.height()), contentHeight = parent.get(0).scrollHeight, maxScroll = contentHeight - viewportHeight, startView = 0, endView = startView + viewportHeight, halfView = endView - viewportHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.position().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    console.log(rangeStart, start, rangeEnd), start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, 
                    anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            function updateFromMainScroll(scrollValue) {
                var windowHeight = $window.innerHeight, documentHeight = body.height(), maxScroll = documentHeight - windowHeight, startView = scrollValue;
                startView || (startView = 0);
                var endView = startView + windowHeight, halfView = endView - windowHeight / 2, elementHeight = $element.outerHeight(), elementStart = $element.offset().top, elementEnd = elementStart + elementHeight, elementHalf = elementStart + elementHeight / 4;
                if (onAnchor) {
                    var start = parseInt(startView), rangeStart = parseInt(elementStart), rangeEnd = parseInt(elementHalf);
                    console.log(rangeStart, start, rangeEnd), start >= rangeStart && rangeEnd > start ? anchored || (anchored = !0, 
                    anchored && onAnchor()) : anchored = !1;
                }
                var entirelyViewable = elementStart >= startView && endView >= elementEnd;
                return setScrollContext(entirelyViewable ? "active" : halfView >= elementEnd ? "after" : halfView >= elementStart ? "active" : startView >= maxScroll - 200 ? "active" : "before");
            }
            var onActive, onBefore, onAfter, onAnchor, anchored, currentContext = "";
            $attrs.onActive && (onActive = function() {
                $scope.$eval($attrs.onActive);
            }), $attrs.onAnchor && (onAnchor = function() {
                $scope.$eval($attrs.onAnchor);
            }), $attrs.onAfter && (onAfter = function() {
                $scope.$eval($attrs.onAfter);
            }), $attrs.onBefore && (onBefore = function() {
                $scope.$eval($attrs.onBefore);
            });
            var parent = $element.closest("[scroll-active-parent]"), body = angular.element("body");
            parent.length ? (parent.bind("scroll", updateParentScroll), $timeout(updateParentScroll, 10)) : ($scope.$watch(function() {
                return FluroScrollService.getScroll();
            }, updateFromMainScroll), $timeout(updateFromMainScroll, 10));
        }
    };
}), app.service("FluroScrollService", function($window, $location, $timeout) {
    function updateScroll() {
        var v = this.pageYOffset;
        _value != this.pageYOffset && (_value > v ? controller.direction = "up" : controller.direction = "down", 
        $timeout(function() {
            _value = this.pageYOffset;
        }));
    }
    var controller = {};
    controller.cache = {}, controller.direction = "down";
    var _value = 0;
    angular.element("html,body");
    return controller.setAnchor = function(id) {
        $location.hash("jump-to-" + id);
    }, controller.getAnchor = function() {
        var hash = $location.hash();
        return _.startsWith(hash, "jump-to-") ? hash.substring(8) : hash;
    }, controller.scrollToID = controller.scrollToId = function(id, speed, selector, offset) {
        0 == speed || speed || (speed = "fast");
        var $target = angular.element("#" + id);
        if ($target && $target.offset && $target.offset()) {
            selector || (selector = "body,html");
            var pos = $target.offset().top;
            offset && (pos += Number(offset)), angular.element(selector).animate({
                scrollTop: pos
            }, speed);
        }
    }, controller.scrollToPosition = controller.scrollTo = function(pos, speed, selector, offset) {
        0 == speed || speed || (speed = "fast"), selector || (selector = "body,html"), offset && (pos += Number(offset)), 
        angular.element(selector).animate({
            scrollTop: pos
        }, speed);
    }, controller.get = controller.getScroll = function() {
        return _value;
    }, controller.getMax = function(selector) {
        selector || (selector = "body,html");
        var bodyHeight = angular.element(selector).height(), windowHeight = $window.innerHeight;
        return bodyHeight - windowHeight;
    }, controller.getHalfPoint = function() {
        return $window.innerHeight / 2;
    }, controller.getWindowHeight = function() {
        return $window.innerHeight;
    }, angular.element($window).bind("scroll", updateScroll), updateScroll(), controller;
}), app.service("FluroSEOService", function($rootScope, $location) {
    var controller = {};
    return $rootScope.$watch(function() {
        return controller.siteTitle + " | " + controller.pageTitle;
    }, function() {
        controller.headTitle = "", controller.siteTitle && controller.siteTitle.length ? (controller.headTitle += controller.siteTitle, 
        controller.pageTitle && controller.pageTitle.length && (controller.headTitle += " | " + controller.pageTitle)) : controller.pageTitle && controller.pageTitle.length && (controller.headTitle = controller.pageTitle);
    }), controller.getImageURL = function() {
        var url = controller.defaultImageURL;
        return controller.imageURL && controller.imageURL.length && (url = controller.imageURL), 
        url;
    }, controller.getDescription = function() {
        var description = controller.defaultDescription;
        return controller.description && (description = controller.description), description && description.length ? description : "";
    }, $rootScope.$on("$stateChangeSuccess", function() {
        controller.url = $location.$$absUrl;
    }), $rootScope.$on("$stateChangeStart", function() {
        controller.description = null, controller.imageURL = null;
    }), controller;
}), app.directive("statWidget", function() {
    return {
        restrict: "E",
        scope: {
            item: "=",
            stat: "@",
            myStats: "=?myStats"
        },
        controller: "FluroStatWidgetController",
        transclude: !0,
        link: function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function(clone, $scope) {
                $element.replaceWith(clone);
            });
        }
    };
}), app.controller("FluroStatWidgetController", function($scope, $timeout, FluroContent) {
    $scope.myStats || ($scope.myStats = {}), $scope.processing = !1, $scope.$watch(function() {
        var myStatValue = $scope.myStats["_" + $scope.stat], totalStatValue = 0;
        return $scope.item.stats && $scope.item.stats["_" + $scope.stat] && (totalStatValue = $scope.item.stats["_" + $scope.stat]), 
        myStatValue + "-" + totalStatValue;
    }, function(tally) {
        var myStatValue = $scope.myStats["_" + $scope.stat], totalStatValue = 0;
        $scope.item.stats && $scope.item.stats["_" + $scope.stat] && (totalStatValue = $scope.item.stats["_" + $scope.stat]), 
        $scope.myTotal = myStatValue, $scope.total = totalStatValue;
    }), $scope.toggle = function() {
        if ($scope.item && $scope.stat) {
            $scope.item.stats || ($scope.item.stats = {});
            var prevCount, statName = $scope.stat, itemID = $scope.item._id;
            $scope.myStats && (prevCount = $scope.myStats["_" + statName]);
            var request = FluroContent.endpoint("stat/" + itemID + "/" + statName).update({}).$promise;
            $scope.processing = !0, request.then(function(res) {
                $scope.processing = !1, $timeout(function() {
                    $scope.myStats && (prevCount ? $scope.myStats["_" + statName] = 0 : $scope.myStats["_" + statName] = 1), 
                    $scope.item.stats["_" + statName] = res.total;
                });
            }, function(err) {
                $scope.processing = !1, console.log("Error updating stat", err);
            });
        }
    };
}), app.controller("UserLoginController", function($scope, $http, FluroTokenService, NotificationService) {
    $scope.credentials = {}, $scope.status = "ready", $scope.signup = function(options) {
        $scope.status = "processing";
        var request = FluroTokenService.signup($scope.credentials, options);
        request.then(function(res) {
            $scope.status = "ready", NotificationService.message("Hi " + res.data.firstName);
        }, function(res) {
            $scope.status = "ready", NotificationService.message(String(res.data), "danger");
        });
    }, $scope.login = function(options) {
        $scope.status = "processing";
        var request = FluroTokenService.login($scope.credentials, options);
        request.then(function(res) {
            $scope.status = "ready", NotificationService.message("Welcome back " + res.data.firstName);
        }, function(res) {
            $scope.status = "ready", console.log("FAILED", res), NotificationService.message(String(res.data), "danger");
        });
    };
}), app.directive("hamburger", function() {
    return {
        restrict: "E",
        replace: !0,
        template: '<div class="hamburger"> 		  <span></span> 		  <span></span> 		  <span></span> 		  <span></span> 		</div>',
        link: function($scope, $elem, $attr) {}
    };
}), app.directive("compileHtml", function($compile) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            scope.$watch(function() {
                return scope.$eval(attrs.compileHtml);
            }, function(value) {
                element.html(value), $compile(element.contents())(scope);
            });
        }
    };
}), app.directive("httpSrc", function($http) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var requestConfig = {
                method: "get",
                url: attrs.httpSrc,
                responseType: "arraybuffer",
                cache: "true"
            };
            $http(requestConfig).then(function(res) {
                var b64 = _arrayBufferToBase64(res.data);
                attrs.$set("src", "data:image/jpeg;base64," + b64);
            }, function(err) {
                console.log("HTTP IMAGE LOAD ERROR", err);
            });
        }
    };
}), app.directive("httpBgSrc", function($http, Fluro) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var requestConfig = {
                method: "get",
                url: attrs.httpBgSrc,
                responseType: "arraybuffer",
                cache: "true"
            };
            $http(requestConfig).then(function(res) {
                var b64 = _arrayBufferToBase64(res.data);
                element.css("backgroundImage", "url(data:image/jpeg;base64," + b64 + ")");
            }, function(err) {
                console.log("HTTP IMAGE LOAD ERROR", err);
            });
        }
    };
}), app.directive("infinitePager", function($timeout, $sessionStorage) {
    return {
        restrict: "A",
        link: function($scope, $element, $attr) {
            var perPage = 16;
            $attr.perPage && (perPage = parseInt($attr.perPage)), $scope.pager = {
                currentPage: 0,
                limit: perPage
            }, $scope.pages = [], $scope.$watch($attr.items, function(items) {
                $scope.allItems = items, $scope.allItems && ($scope.pages.length = 0, $scope.pager.currentPage = 0, 
                $scope.totalPages = Math.ceil($scope.allItems.length / $scope.pager.limit) - 1, 
                $scope.updateCurrentPage());
            }), $scope.updateCurrentPage = function() {
                $scope.allItems.length < $scope.pager.limit * ($scope.pager.currentPage - 1) && ($scope.pager.currentPage = 0);
                var start = $scope.pager.currentPage * $scope.pager.limit, end = start + $scope.pager.limit, sliced = _.slice($scope.allItems, start, end);
                $scope.pages.push(sliced);
            };
            var timer;
            $scope.nextPage = function() {
                $scope.pager.currentPage < $scope.totalPages ? ($timeout.cancel(timer), timer = $timeout(function() {
                    $scope.pager.currentPage = $scope.pager.currentPage + 1, $scope.updateCurrentPage();
                })) : $scope.updateCurrentPage();
            };
        }
    };
}), app.directive("longTextWrap", function() {
    return {
        restrict: "E",
        transclude: !0,
        template: '<div class="long-text-wrap" ng-class="{expanded:ltSettings.expanded}" ><div class="long-text-wrap-inner" ng-transclude></div><a class="long-text-wrap-link" ng-click="ltSettings.expanded = !ltSettings.expanded"><div><span>Read more<span><i class="fa fa-angle-down"></i></div></a></div>',
        controller: function($scope) {
            $scope.ltSettings = {};
        }
    };
}), app.filter("capitalise", function() {
    return function(str) {
        return _.upperCase(str);
    };
}), app.filter("commaSummary", function() {
    return function(arrayOfObjects, limit) {
        limit || (limit = 20);
        var names = _.chain(arrayOfObjects).map(function(object) {
            return object.title;
        }).compact().value(), string = names.join(", ");
        return string.length >= limit && (string = string.substr(0, limit) + "..."), string;
    };
}), app.filter("dateInitials", function($rootScope) {
    return function(string) {
        switch (string) {
          case "year":
            return "yr";

          case "day":
            return "day";

          case "week":
            return "wk";

          case "month":
            return "mo";

          case "once":
            return "once";

          default:
            return string[0] + string[1];
        }
    };
}), app.filter("filesize", function() {
    return function(bytes) {
        var sizes = [ "Bytes", "kb", "mb", "gb", "tb" ];
        if (0 == bytes) return "0 Byte";
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + "" + sizes[i];
    };
}), app.filter("maplink", function() {
    return function(location) {
        var pieces = [];
        location.title && location.title.length && pieces.push(location.title), location.addressLine1 && location.addressLine1.length && pieces.push(location.addressLine1), 
        location.addressLine2 && location.addressLine2.length && pieces.push(location.addressLine2), 
        location.state && location.state.length && pieces.push(location.state), location.suburb && location.suburb.length && pieces.push(location.suburb), 
        location.postalCode && location.postalCode.length && pieces.push(location.postalCode);
        var url = "https://www.google.com/maps/place/" + pieces.join("+");
        return url;
    };
}), app.filter("readableDate", function() {
    return function(event, style) {
        if (event && event.startDate) {
            var startDate = new Date(event.startDate), endDate = new Date(event.endDate), noEndDate = startDate.format("g:ia j M Y") == endDate.format("g:ia j M Y"), sameYear = startDate.format("Y") == endDate.format("Y"), sameMonth = startDate.format("M Y") == endDate.format("M Y"), sameDay = startDate.format("j M Y") == endDate.format("j M Y");
            switch (style) {
              case "short":
                return noEndDate ? startDate.format("j M") : sameDay ? startDate.format("j M") : sameMonth ? startDate.format("j") + "-" + endDate.format("j M") : sameYear ? startDate.format("j") + "-" + endDate.format("j M") : startDate.format("j M Y") + " - " + endDate.format("j M Y");

              default:
                return noEndDate ? startDate.format("g:ia l j M Y") : sameDay ? startDate.format("g:ia") + " - " + endDate.format("g:ia l j M Y") : sameMonth ? startDate.format("j") + "-" + endDate.format("j M Y") : sameYear ? startDate.format("j M") + " - " + endDate.format("j M Y") : startDate.format("j M Y") + " - " + endDate.format("j M Y");
            }
        }
    };
}), app.filter("readableDuration", function() {
    return function(duration) {
        var hour = 0, min = 0, sec = 0;
        duration && (duration >= 60 ? (min = Math.floor(duration / 60), sec = duration % 60) : sec = duration, 
        min >= 60 && (hour = Math.floor(min / 60), min -= 60 * hour));
        var out = "";
        return hour && (out += hour + " hr", out += hour > 1 ? "s " : " "), min && (out += min + " min", 
        out += min > 1 ? "s " : " "), sec && !hour && (out += sec + " sec", out += sec > 1 ? "s " : " "), 
        out.trim();
    };
}), app.filter("reword", function($rootScope) {
    console.log("Get Translations");
    var translations = _.get($rootScope.applicationData, "translations");
    return function(string) {
        return _.each(translations, function(set) {
            string = string.replace(new RegExp(set.from, "g"), set.to);
        }), string;
    };
}), app.filter("timeago", function() {
    return function(date) {
        return moment(date).fromNow();
    };
}), app.service("PathfinderService", function() {
    var service = {};
    return service.extract = function(model) {
        var flat = service.flatten(model);
        return _.chain(flat).map(function(value, key) {
            var depth = key.split(".").length, indent = "";
            _.times(depth - 1, function() {
                indent += "      ";
            });
            var matchObject = _.get(model, key), type = typeof matchObject, readable = key + " - " + value;
            if (_.isArray(matchObject) && (type = "array", readable = key + " (multiple items)"), 
            _.isString(matchObject)) {
                var momentDate = moment(matchObject, moment.ISO_8601, !0), isValid = momentDate.isValid();
                isValid && (matchObject = momentDate.toDate());
            }
            return _.isDate(matchObject) && (type = "date"), {
                value: value,
                key: key,
                depth: depth,
                type: type,
                indent: indent,
                readable: readable
            };
        }).value();
    }, service.flatten = function(obj) {
        function _route(prefix, value) {
            var i, len, type, keys, circularCheck, loc;
            if (null == value) {
                if ("" === prefix) return;
                return void (flattened[prefix] = null);
            }
            if (type = typeof value, "object" != typeof value) flattened[prefix] = value; else {
                if (circularCheck = circlular.indexOf(value), circularCheck >= 0) return loc = circLoc[circularCheck] || "this", 
                void (flattened[prefix] = "[Circular (" + loc + ")]");
                if (circlular.push(value), circLoc.push(prefix), Array.isArray(value)) {
                    for (len = value.length, _route(prefix), len > 10 && (len = 10), i = 0; len > i; i++) _route(prefix + "[" + i + "]", value[i]);
                    return;
                }
                for (keys = Object.keys(value), len = keys.length, prefix && (prefix += "."), 0 == len && _route(prefix, null), 
                i = 0; len > i; i++) _route(prefix + keys[i], value[keys[i]]);
            }
        }
        var flattened = {}, circlular = [], circLoc = [];
        return _route("", obj), flattened;
    }, service;
}), HomeController.resolve = {
    contacts: function(FluroContent) {
        return FluroContent.resource("contact").query({
            fields: [ "title", "data", "realms", "tags" ]
        }).$promise;
    },
    teams: function(FluroContent) {
        return FluroContent.resource("team").query({
            fields: [ "title", "definition" ],
            allDefinitions: !0
        }).$promise;
    },
    tags: function(FluroContent) {
        return FluroContent.resource("tag").query({
            fields: [ "title", "_id" ]
        }).$promise;
    },
    definitions: function(FluroContent) {
        return FluroContent.endpoint("defined/types/contactdetail").query().$promise;
    },
    seo: function(FluroSEOService, Asset, $rootScope) {
        return FluroSEOService.pageTitle = null, !0;
    }
}, HomeController.data = {}, app.controller("HomeController", HomeController), LoginPageController.resolve = {
    seo: function(FluroSEOService) {
        return FluroSEOService.pageTitle = "Sign in", !0;
    }
}, LoginPageController.data = {
    denyAuthenticated: !0
}, app.controller("LoginPageController", LoginPageController), MyAccountController.resolve = {
    seo: function(FluroSEOService, $rootScope) {
        return FluroSEOService.pageTitle = $rootScope.user.firstName, !0;
    },
    purchases: function(FluroContent) {
        return FluroContent.endpoint("my/purchases", !1, !0).query({
            populateLicenses: !0
        }).$promise;
    },
    posts: function(FluroContentRetrieval, $rootScope) {
        if (!$rootScope.user || !$rootScope.user.persona) return [];
        var queryDetails = {
            _type: "post",
            definition: {
                $in: [ "comment", "note" ]
            },
            managedAuthor: $rootScope.user.persona
        }, selectFields = "title parent definition created", promise = FluroContentRetrieval.query(queryDetails, null, null, {
            noCache: !0,
            select: selectFields
        }, null);
        return promise;
    },
    paymentMethods: function($q, FluroContent, PurchaseService) {
        var deferred = $q.defer(), paymentGateway = PurchaseService.applicationPaymentGateway();
        return paymentGateway ? PurchaseService.retrievePaymentMethods(paymentGateway._id) : (deferred.resolve([]), 
        deferred.promise);
    }
}, MyAccountController.data = {
    requireLogin: !0
}, app.controller("MyAccountController", MyAccountController), app.controller("PaymentMethodReplaceController", function($scope, $state, $timeout, FluroContent, PurchaseService) {
    $scope.settings = {
        addCard: {}
    }, $scope.errors = {}, $scope.invalid = !1, $scope.hasErrors = function() {
        return _.keys($scope.errors).length;
    }, $scope.replaceWithNewCard = function(purchase, newCard) {
        function cardCreateSuccess(createdMethod) {
            console.log("Card Create Success", createdMethod), PurchaseService.replacePaymentMethod(purchase, createdMethod).then(function(res) {
                purchase.method = createdMethod, $scope.settings.state = "default";
            }, cardReplaceError);
        }
        function cardReplaceError(err) {
            return $scope.settings.state = "error", err.message ? $scope.settings.errorMessage = err.message : err.error ? err.error.message ? $scope.settings.errorMessage = err.error.message : $scope.settings.errorMessage = err.error : err;
        }
        $scope.settings.state = "processing", PurchaseService.createPaymentMethod(newCard).then(cardCreateSuccess, cardReplaceError);
    }, $scope.$watch("settings.addCard", function(data) {
        $scope.errors = {}, data.name && data.name.length || ($scope.errors.cardname = "Card name is required"), 
        data.number && data.number.length || ($scope.errors.cardnumber = "Card number is required"), 
        data.exp_month && data.exp_month.length || ($scope.errors.exp_month = "Card expiry month is required"), 
        data.exp_year && data.exp_year.length || ($scope.errors.exp_year = "Card expiry year is required"), 
        data.cvc && data.cvc.length || ($scope.errors.cvc = "Card Validation number is required");
        var hasErrors = _.keys($scope.errors).length;
        hasErrors ? $scope.invalid = !0 : $scope.invalid = !1;
    }, !0);
}), ResetPasswordPageController.resolve = {
    token: function($stateParams) {
        return $stateParams.token;
    },
    user: function($q, token, FluroTokenService) {
        var deferred = $q.defer(), request = FluroTokenService.retrieveUserFromResetToken(token, {
            application: !0
        });
        return request.then(function(res) {
            return deferred.resolve(res.data);
        }, function(err) {
            return deferred.resolve(err.data);
        }), deferred.promise;
    },
    seo: function(FluroSEOService) {
        return FluroSEOService.pageTitle = "Reset Password", !0;
    }
}, app.controller("ResetPasswordPageController", ResetPasswordPageController), SearchPageController.resolve = {
    seo: function(FluroSEOService) {
        return FluroSEOService.pageTitle = "Search", !0;
    },
    articles: function(FluroContent) {
        return FluroContent.resource("article").query().$promise;
    }
}, SearchPageController.data = {
    requireLogin: !0
}, app.controller("SearchPageController", SearchPageController), SignupPageController.resolve = {
    seo: function(FluroSEOService) {
        return FluroSEOService.pageTitle = "Sign up", !0;
    }
}, SignupPageController.data = {
    denyAuthenticated: !0
}, app.controller("SignupPageController", SignupPageController), angular.module("fluro").run([ "$templateCache", function($templateCache) {
    "use strict";
    $templateCache.put("admin-date-select/admin-date-select.html", '<div class=dateselect ng-class={open:settings.open}><div class=btn-group><a class="btn btn-default" ng-class={active:settings.open} ng-click="settings.open = !settings.open"><i class="fa fa-calendar"></i> <span ng-bind-html="readable | trusted"></span></a></div><dpiv class=popup><div class=datetime><div uib-datepicker class=datepicker datepicker-options=datePickerOptions ng-model=settings.dateModel></div></div></dpiv></div>'), 
    $templateCache.put("admin-persona-image-replace/persona-image-replace-form.html", '<div><div ng-switch=settings.state><div ng-switch-default><div class="btn btn-default btn-file btn-sm btn-block"><span>Upload photo</span> <input accept=image/jpeg,image/png,image/gif id=file type=file name=file></div></div><div ng-switch-when=processing><i class="fa fa-fw fa-spinner fa-spin"></i> Uploading</div><div ng-switch-when=complete class=brand-success><i class="fa fa-fw fa-check"></i> Photo was updated successfully</div><div ng-switch-when=error class=brand-warning><div><i class="fa fa-fw fa-warning"></i> {{settings.errorMessage}}</div><a class="btn btn-default btn-sm" ng-click="settings.state = \'form\'"><span>Back</span></a></div></div></div>'), 
    $templateCache.put("extended-field-render/extended-field-render.html", '<div class="extended-field-render form-group"><label ng-if="field.type != \'group\'">{{field.title}}</label><div field-transclude></div></div>'), 
    $templateCache.put("extended-field-render/field-types/multiple-value.html", '<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class="content-list list-group"><div class="list-group-item clearfix" ng-repeat="item in model[field.key]"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if="item._type == \'image\'" ng-src="{{$root.getThumbnailUrl(item._id)}}"> <i ng-if="item._type != \'image\'" class="fa fa-{{item._type}}"></i> <i ng-if="item.definition == \'song\'" class="fa fa-music" style=padding-right:10px></i> <span>{{item.title}}</span></div></a><div class="actions pull-right btn-group"><a class="btn btn-tiny btn-xs" ng-if="item.assetType == \'upload\'" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class="fa fa-download"></i></a> <a class="btn btn-tiny btn-xs" ng-if=canEdit(item) ng-click=editInModal(item)><i class="fa fa-edit"></i></a></div></div></div></div><div ng-switch-default><ul><li ng-repeat="value in model[field.key]">{{value}}</li></ul></div></div>'), 
    $templateCache.put("extended-field-render/field-types/value.html", '<div ng-switch=field.type><div class=content-select ng-switch-when=reference><div class="content-list list-group"><div class="list-group-item clearfix" ng-init="item = model[field.key]"><a ui-sref=viewContent({id:item._id})><div class=pull-left><img ng-if="item._type == \'image\'" ng-src="{{$root.getThumbnailUrl(item._id)}}"> <i ng-if="item._type != \'image\'" class="fa fa-{{item._type}}"></i> <span>{{item.title}}</span></div></a><div class="actions pull-right btn-group"><a class="btn btn-tiny btn-xs" ng-if="item.assetType == \'upload\'" target=_blank ng-href={{$root.getDownloadUrl(item._id)}}><i class="fa fa-download"></i></a></div></div></div></div><div ng-switch-when=date>{{model[field.key] | formatDate:\'j M Y\'}}</div><div ng-switch-when=image><img ng-src="{{$root.asset.imageUrl(item._id)}}"></div><div ng-switch-default><div ng-bind-html="model[field.key] | trusted"></div></div></div>'), 
    $templateCache.put("fluro-filter-block/fluro-filter-block.html", '<div><div class=filter-block ng-class={expanded:isExpanded()} ng-if=filterItems.length><div class=filter-block-title ng-click=clicked()><i class="fa fa-fw fa-angle-right pull-right" ng-class="{\'fa-rotate-90\':isExpanded()}"></i> <span>{{ title }}</span><div class="small brand-primary" style="padding-top: 5px" ng-show=hasFilters()>{{getTitle()}}</div></div><div class=filter-block-options><a class=filter-block-option ng-show=hasFilters() ng-click="toggleFilter(); settings.expanded = false;"><span>Any {{title}}</span></a> <a class=filter-block-option ng-click=toggleFilter(filterItem) ng-class="{active:isActiveFilter(filterItem), inactive:!isActiveFilter(filterItem) && hasFilters()}" ng-repeat="filterItem in filterItems track by filterItem._id"><span>{{filterItem.title}}</span></a></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/button-select/fluro-button-select.html", '<div id={{options.id}} class="button-select {{to.definition.directive}}-buttons" ng-model=model[options.key]><a ng-repeat="(key, option) in to.options" ng-class={active:contains(option.value)} class="btn btn-default" id="{{id + \'_\'+ $index}}" ng-click=toggle(option.value)><span>{{option.name}}</span><i class="fa fa-check"></i></a></div>'), 
    $templateCache.put("fluro-interaction-form/custom.html", "<div ng-model=model[options.key] compile-html=to.definition.template></div>"), 
    $templateCache.put("fluro-interaction-form/date-select/fluro-date-select.html", '<div ng-controller=FluroDateSelectController><div class=input-group><input class=form-control datepicker-popup={{format}} ng-model=model[options.key] is-open=opened min-date=to.minDate max-date=to.maxDate datepicker-options=dateOptions date-disabled="disabled(date, mode)" ng-required=to.required close-text="Close"> <span class=input-group-btn><button type=button class="btn btn-default" ng-click=open($event)><i class="fa fa-calendar"></i></button></span></div></div>'), 
    $templateCache.put("fluro-interaction-form/dob-select/fluro-dob-select.html", "<div class=fluro-interaction-dob-select><dob-select ng-model=model[options.key] hide-age=to.params.hideAge hide-dates=to.params.hideDates></dob-select></div>"), 
    $templateCache.put("fluro-interaction-form/embedded/fluro-embedded.html", '<div class=fluro-embedded-form><div class=form-multi-group ng-if="to.definition.maximum != 1"><div class="panel panel-default" ng-init="fields = copyFields(); dataFields = copyDataFields(); " ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><a ng-if=canRemove() class="btn btn-danger btn-sm pull-right" ng-click="model[options.key].splice($index, 1)"><span>Remove {{to.label}}</span><i class="fa fa-times"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form><formly-form model=entry.data fields=dataFields></formly-form></div></div><a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form><formly-form model=model[options.key].data fields=options.data.dataFields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/field-errors.html", '<div class=field-errors ng-if="fc.$touched && fc.$invalid"><div ng-show=fc.$error.required class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> {{to.label}} is required.</div><div ng-show=fc.$error.validInput class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> <span ng-if=!to.errorMessage.length>\'{{fc.$viewValue}}\' is not a valid value</span> <span ng-if=to.errorMessage.length>{{to.errorMessage}}</span></div><div ng-show=fc.$error.email class="alert alert-danger" role=alert><span class="fa fa-exclamation" aria-hidden=true></span> <span class=sr-only>Error:</span> <span>\'{{fc.$viewValue}}\' is not a valid email address</span></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-interaction-input.html", '<div class="fluro-input form-group" scroll-active ng-class="{\'fluro-valid\':isValid(), \'fluro-dirty\':isDirty, \'fluro-invalid\':!isValid()}"><label><i class="fa fa-check" ng-if=isValid()></i><i class="fa fa-exclamation" ng-if=!isValid()></i><span>{{field.title}}</span></label><div class="error-message help-block"><span ng-if=field.errorMessage>{{field.errorMessage}}</span> <span ng-if=!field.errorMessage>Please provide valid input for this field</span></div><span class=help-block ng-if="field.description && field.type != \'boolean\'">{{field.description}}</span><div class=fluro-input-wrapper></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-terms.html", '<div class=terms-checkbox><div class=checkbox><label><input type=checkbox ng-model="model[options.key]"> {{to.definition.params.storeData}}</label></div></div>'), 
    $templateCache.put("fluro-interaction-form/fluro-web-form.html", '<div class=fluro-interaction-form><div ng-if=!correctPermissions class=form-permission-warning><div class="alert alert-warning small"><i class="fa fa-warning"></i> <span>You do not have permission to post {{model.plural}}</span></div></div><div ng-if="promisesResolved && correctPermissions"><div ng-if=debugMode><div class="btn-group btn-group-justified"><a ng-click="vm.state = \'ready\'" class="btn btn-default">State to ready</a> <a ng-click="vm.state = \'complete\'" class="btn btn-default">State to complete</a> <a ng-click=reset() class="btn btn-default">Reset</a></div><hr></div><div ng-show="vm.state != \'complete\'"><form novalidate ng-submit=vm.onSubmit()><formly-form model=vm.model fields=vm.modelFields form=vm.modelForm options=vm.options><div ng-if=model.data.recaptcha><div recaptcha-render></div></div><div class="form-error-summary form-client-error alert alert-warning" ng-if="vm.modelForm.$invalid && !vm.modelForm.$pristine"><div class=form-error-summary-item ng-repeat="field in errorList" ng-if=field.formControl.$invalid><i class="fa fa-exclamation"></i> <span ng-if=field.templateOptions.definition.errorMessage.length>{{field.templateOptions.definition.errorMessage}}</span> <span ng-if=!field.templateOptions.definition.errorMessage.length>{{field.templateOptions.label}} has not been provided.</span></div></div><div ng-switch=vm.state><div ng-switch-when=sending><a class="btn btn-primary" ng-disabled=true><span>Processing</span> <i class="fa fa-spinner fa-spin"></i></a></div><div ng-switch-when=error><div class="form-error-summary form-server-error alert alert-danger" ng-if=processErrorMessages.length><div class=form-error-summary-item ng-repeat="error in processErrorMessages track by $index"><i class="fa fa-exclamation"></i> <span>Error processing your submission: {{error}}</span></div></div><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>Try Again</span> <i class="fa fa-angle-right"></i></button></div><div ng-switch-default><button type=submit class="btn btn-primary" ng-disabled=!readyToSubmit><span>{{submitLabel}}</span> <i class="fa fa-angle-right"></i></button></div></div></formly-form></form></div><div ng-show="vm.state == \'complete\'"><div compile-html=transcludedContent></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/nested/fluro-nested.html", '<div><div class=form-multi-group ng-if="to.definition.maximum != 1"><div class="panel panel-default" ng-init="fields = copyFields()" ng-repeat="entry in model[options.key] track by $index"><div class="panel-heading clearfix"><a ng-if=canRemove() class="btn btn-danger btn-sm pull-right" ng-click="model[options.key].splice($index, 1)"><span>Remove {{to.label}}</span><i class="fa fa-times"></i></a><h5>{{to.label}} {{$index + 1}}</h5></div><div class=panel-body><formly-form model=entry fields=fields></formly-form></div></div><a class="btn btn-primary btn-sm" ng-if=canAdd() ng-click=addAnother()><span>Add <span ng-if=model[options.key].length>another</span> {{to.label}}</span><i class="fa fa-plus"></i></a></div><div ng-if="to.definition.maximum == 1 && options.key"><formly-form model=model[options.key] fields=options.data.fields></formly-form></div></div>'), 
    $templateCache.put("fluro-interaction-form/order-select/fluro-order-select.html", '<div id={{options.id}} class=fluro-order-select><div ng-if=selection.values.length><p class=help-block>Drag to reorder your choices</p></div><div class=list-group as-sortable=dragControlListeners formly-skip-ng-model-attrs-manipulator ng-model=selection.values><div class="list-group-item clearfix" as-sortable-item ng-repeat="item in selection.values"><div class=pull-left as-sortable-item-handle><i class="fa fa-arrows order-select-handle"></i> <span class="order-number text-muted">{{$index+1}}</span> <span>{{item}}</span></div><div class="pull-right order-select-remove" ng-click=deselect(item)><i class="fa fa-times"></i></div></div></div><div ng-if=canAddMore()><p class=help-block>Choose by selecting options below</p><select class=form-control ng-model=selectBox.item ng-change=selectUpdate()><option ng-repeat="(key, option) in to.options | orderBy:\'value\'" ng-if=!contains(option.value) value={{option.value}}>{{option.value}}</option></select></div></div>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-method.html", '<hr><div class=payment-method-select><div ng-if=!settings.showOptions><h3 class=clearfix>{{selected.method.title}} <em class="pull-right small" ng-click="settings.showOptions = !settings.showOptions">Other payment options <i class="fa fa-angle-right"></i></em></h3></div><div ng-if=settings.showOptions><h3 class=clearfix>Select payment method <em ng-click="settings.showOptions = false" class="pull-right small">Back <i class="fa fa-angle-up"></i></em></h3><div class="payment-method-list list-group"><div class="payment-method-list-item list-group-item" ng-class="{active:method == selected.method}" ng-click=selectMethod(method) ng-repeat="method in methodOptions"><h5 class=title>{{method.title}}</h5></div></div></div><div ng-if=!settings.showOptions><div ng-if="selected.method.key == \'card\'"><formly-form model=model fields=options.data.fields></formly-form></div><div ng-if="selected.method == method && selected.method.description.length" ng-repeat="method in methodOptions"><div compile-html=method.description></div></div></div></div><hr>'), 
    $templateCache.put("fluro-interaction-form/payment/payment-summary.html", '<hr><div class=payment-summary><h3>Payment details</h3><div class=form-group><div ng-if=modifications.length class=payment-running-total><div class="row payment-base-row"><div class=col-xs-6><strong>Base Price</strong></div><div class="col-xs-3 col-xs-offset-3">{{paymentDetails.amount / 100 | currency}}</div></div><div class="row text-muted" ng-repeat="mod in modifications"><div class=col-xs-6><em>{{mod.title}}</em></div><div class="col-xs-3 text-right"><em>{{mod.description}}</em></div><div class=col-xs-3><em class=text-muted>{{mod.total / 100 | currency}}</em></div></div><div class="row payment-total-row"><div class=col-xs-6><h4>Total</h4></div><div class="col-xs-3 col-xs-offset-3"><h4>{{calculatedTotal /100 |currency}} <span class="text-uppercase text-muted">{{paymentDetails.currency}}</span></h4></div></div></div><div class=payment-amount ng-if=!modifications.length>{{calculatedTotal /100 |currency}} <span class=text-uppercase>({{paymentDetails.currency}})</span></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-item.html", '<a class=clearfix><i class="fa fa-{{match.model._type}}"></i> <span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span> <span ng-if="match.model._type == \'event\' || match.model._type == \'plan\'" class="small text-muted">// {{match.model.startDate | formatDate:\'jS F Y - g:ia\'}}</span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select-value.html", '<a class=clearfix><span ng-bind-html="match.label | trusted | typeaheadHighlight:query"></span></a>'), 
    $templateCache.put("fluro-interaction-form/search-select/fluro-search-select.html", '<div class=fluro-search-select><div ng-if="to.definition.type == \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="item in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(item)></i> {{item.title}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{selection.value.title}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-item.html typeahead-on-select=select($item) placeholder=Search typeahead="item.title for item in retrieveReferenceOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div><div ng-if="to.definition.type != \'reference\'"><div class=list-group ng-if="multiple && selection.values.length"><div class=list-group-item ng-repeat="value in selection.values"><i class="fa fa-times pull-right" ng-click=deselect(value)></i> {{getValueLabel(value)}}</div></div><div class=list-group ng-if="!multiple && selection.value"><div class="list-group-item clearfix"><i class="fa fa-times pull-right" ng-click=deselect(selection.value)></i> {{getValueLabel(selection.value)}}</div></div><div ng-if=canAddMore()><div class=input-group><input class=form-control formly-skip-ng-model-attrs-manipulator ng-model=proposed.value typeahead-template-url=fluro-interaction-form/search-select/fluro-search-select-value.html typeahead-on-select=select($item.value) placeholder=Search typeahead="item.name for item in retrieveValueOptions($viewValue)" typeahead-loading="search.loading"><div class=input-group-addon ng-if=!search.loading ng-click="search.terms = \'\'"><i class=fa ng-class="{\'fa-search\':!search.terms.length, \'fa-times\':search.terms.length}"></i></div><div class=input-group-addon ng-if=search.loading><i class="fa fa-spin fa-spinner"></i></div></div></div></div></div>'), 
    $templateCache.put("fluro-interaction-form/value/value.html", "<div class=fluro-interaction-value style=display:none><pre>{{model[options.key] | json}}</pre></div>"), 
    $templateCache.put("fluro-preloader/fluro-preloader.html", '<div class="preloader {{preloader.class}}"><div class=preloader-bg></div><div class=preloader-fg><div class=spinner><svg version=1.1 id=loader-1 xmlns=http://www.w3.org/2000/svg xmlns:xlink=http://www.w3.org/1999/xlink x=0px y=0px width=40px height=40px viewbox="0 0 50 50" style="enable-background:new 0 0 50 50" xml:space=preserve><path fill=#000 d=M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z transform="rotate(170 25 25)"><animatetransform attributetype=xml attributename=transform type=rotate from="0 25 25" to="360 25 25" dur=0.6s repeatcount=indefinite></animatetransform></path></svg></div></div></div>'), 
    $templateCache.put("fluro-purchase-service/payment-modal.html", '<div class="panel panel-default"><div ng-switch=settings.state><div ng-switch-when=processing class="panel-body text-center"><div class="h1 text-center"><i class="fa fa-spinner fa-spin"></i></div></div><div ng-switch-when=error><div class=panel-heading><h3 class=panel-title>{{product.title}}</h3></div><div class=panel-body><h5>Purchase Error</h5><pre>{{settings.errorMessage | json}}</pre></div><div class=panel-footer><div class=row><div class=col-xs-6><a class="btn btn-default" ng-click="settings.state = \'ready\'"><i class="fa fa-angle-left"></i> <span>Try Again</span></a></div><div class="col-xs-6 text-right"><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a></div></div></div></div><div ng-switch-when=purchased><div class=panel-heading><h3 class=panel-title>Purchase Complete</h3></div><div class="panel-body text-center"><i class="fa fa-check fa-3x"></i><div class=lead>You have purchased {{product.title}}.</div></div><div class=panel-footer><a class="btn btn-default btn-block" ng-click=$dismiss()><span>Ok</span></a></div></div><div ng-switch-default><div class=panel-heading><h3 class=panel-title>{{product.title}}</h3></div><div ng-if=!product.amount><div class=panel-body><p>{{product.title}} is available free.<br>Click below to add it to your library.</p></div><div class=panel-footer><div class=row><div class=col-xs-6><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a></div><div class="col-xs-6 text-right"><a ng-click=purchaseFree(product._id) class="btn btn-primary"><span>Add to my library</span> <i class="fa fa-angle-right"></i></a></div></div></div></div><div ng-if=product.amount><div ng-if="methods.length && !settings.newCard"><div class=panel-body><div class=list-group><div class=list-group-item ng-class="{active:settings.selectedMethod == method}" ng-repeat="method in methods track by method._id" ng-click="settings.selectedMethod = method"><i class="fa fa-credit-card"></i> {{method.title}}</div><div class=list-group-item ng-click="settings.newCard = true">Add another card</div></div></div><div class=panel-footer><div class=row><div class=col-xs-6><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a></div><div class="col-xs-6 text-right"><a ng-click="purchaseWithMethod(product._id, settings.selectedMethod)" ng-show=settings.selectedMethod class="btn btn-primary"><span>Purchase {{product.amount/100 | currency}}</span> <i class="fa fa-angle-right"></i></a></div></div></div></div><div ng-if="!methods.length || settings.newCard"><div class=panel-body><div class=form-group><label>Name on Card *</label><input placeholder="Card Name eg. (Kevin Durant)" class=form-control ng-model="card.name"></div><div class=form-group><label>Card Number *</label><input placeholder="Card Number eg. (0000-0000-0000-0000)" class=form-control ng-model="card.number"></div><div class=row><div class="form-group col-xs-4"><label>Exp Month * <em class="small text-muted">MM</em></label><input placeholder=MM class=form-control ng-model="card.exp_month"></div><div class="form-group col-xs-4"><label>Exp Year * <em class="small text-muted">YYYY</em></label><input placeholder=YYYY class=form-control ng-model="card.exp_year"></div><div class="form-group col-xs-4"><label>CVC *</label><input placeholder=CVC class=form-control ng-model="card.cvc"></div></div><div class=form-group><div class=checkbox><label><input type=checkbox ng-model=card.saveCard> Remember this card for next time</label></div></div></div><div class=panel-footer><div class=row><div class=col-xs-6><a class="btn btn-default" ng-click=$dismiss()><span>Cancel</span></a></div><div class="col-xs-6 text-right"><a ng-click="purchaseWithCard(product._id, card)" class="btn btn-primary"><span>Purchase {{product.amount/100 | currency}}</span> <i class="fa fa-angle-right"></i></a></div></div></div></div></div></div></div></div>'), 
    $templateCache.put("routes/home/home.html", '<div ng-switch=state><div ng-switch-when=processing><div class=text-wrap><h4 style="padding: 50px 0" ng-show="progress.current < progress.total"><i class="fa fa-spinner fa-spin"></i> <span>Remapping</span></h4><h4 style="padding: 50px 0" ng-hide="progress.current < progress.total"><i class="fa fa-check brand-success"></i> <span>Remap complete</span></h4><uib-progress max=progress.total><uib-bar value=progress.current type=success><span>{{progress.current / progress.total * 100 | number:0}}%</span></uib-bar></uib-progress><div><strong>Contacts Processed</strong>: {{progress.current}} / {{progress.total}}</div><div><strong>Detail Sheets Created</strong>: {{progress.sheet}}</div><div><strong>Teams Created</strong>: {{progress.team}}</div><div><strong>Tags Added</strong>: {{progress.tagAdded}}</div></div></div><div ng-switch-default><div class=wrapper-sm><div class=container-fluid><label class=smallcaps style="font-size: 0.8em">{{::$root.user.account.title }}</label><h3 style=margin-top:0>{{contacts.length}} contacts have metadata</h3><div>Select a contact detail and field to move data to, select a team to join, or tags to add based on metadata on a contact</div></div></div><div class=table-responsive><table class=table><thead><tr><th>From Key</th><th>To</th><th>Select</th><th>Field</th></tr></thead><tbody><tr ng-repeat="option in options" ng-class="{inactive:!option.to.length || (!option.definition && !option.team&& !option.tag)}"><td class=col-xs-3><strong>contact.data[\'{{option.from}}\']</strong><div class="small text-muted">Values include: {{option.exampleString}}</div></td><td class=col-xs-3><select class=form-control ng-model=option.to><option value="">None</option><option value=definition>Move to Contact Detail</option><option value=team>Add to Team</option><option value=tag>Add Tags</option></select></td><td class=col-xs-3><select class=form-control ng-if="option.to == \'definition\'" ng-model=option.definition ng-options="definition as definition.title for definition in definitions track by definition._id"><option value="">None</option></select><select class=form-control ng-if="option.to == \'team\'" ng-model=option.team><option value="">None</option><option value=key>Key as Team Name ({{option.from}})</option><option value=value>Values as Team Name ({{option.exampleString}})</option></select><select class=form-control ng-if="option.to == \'tag\'" ng-model=option.tag><option value="">None</option><option value=key>Key as Tag ({{option.from}})</option><option value=value>Values as Tag ({{option.exampleString}})</option></select></td><td class=col-xs-3><select class=form-control ng-model=option.field ng-options="field as field.title for field in option.definition.fields track by field.key" ng-if=option.definition><option value="">{{option.definition.definitionName}}.data[\'{{option.from}}\'] (same key)</option></select><div ng-if="option.to == \'team\'" ng-switch=option.team><div ng-switch-when=key>New team with title \'{{option.from}}\'</div><div ng-switch-when=value>New teams with titles like \'{{option.exampleString}}\'</div></div><div ng-if="option.to == \'tag\'" ng-switch=option.tag><div ng-switch-when=key>New tag with title \'{{option.from}}\'</div><div ng-switch-when=value>New tags with titles like \'{{option.exampleString}}\'</div></div></td></tr></tbody></table></div><div class=wrapper-sm><div class="container-fluid text-center"><div class=text-wrap><p>If you have select all of the pieces of data to move, click the button below to start processing</p><a class="btn btn-primary" ng-click=start()><span>Let\'s do it!</span></a></div></div></div></div></div>'), 
    $templateCache.put("routes/login.forgot/forgot.html", '<div class=wrapper><div class=container ng-switch=settings.state><div class=text-wrap-sm><div class="alert alert-danger" ng-show=settings.errorMessage><i class="fa fa-warning"></i> <span>{{settings.errorMessage}}</span></div><div class=text-center ng-switch-when=processing><i class="fa lnr lnr-sync fa-spin fa-3x"></i><h4>Sending reset email</h4></div><div class=text-center ng-switch-when=complete><h4>Reset Email Sent</h4><p>We just sent an reset link to {{credentials.username}}. Please make sure you check your spam</p><a class="btn btn-primary btn-block" ng-click=backToLogin()><span>Back to login</span></a></div><div ng-switch-default><div class=wrapper-title><h3>Forgot Password</h3><p class=help-block>Enter your email below and we\'ll send a link for you to reset your password</p></div><form ng-submit=sendResetToken()><div class=form-group-underline><input ng-model=credentials.username type=email name="credentialsEmail"><label for=credentialsEmail><span>Email Address</span></label></div><div class=wrapper-xs><button class="btn btn-block btn-primary"><span>Send reset link</span></button> <a class="btn btn-block btn-link" ui-sref=login.form>Back to login</a></div></form></div></div></div></div>'), 
    $templateCache.put("routes/login/login.form.html", '<div class=wrapper><div class=container ng-switch=settings.state><div class=text-wrap-sm><div class="alert alert-danger" ng-show=settings.error><i class="fa fa-warning"></i> <span>{{settings.error}}</span></div><div class=text-center ng-switch-when=processing><i class="fa lnr lnr-sync fa-spin fa-3x"></i><h4>Signing you in</h4></div><div class=text-center ng-switch-when=complete><h4>Log in Complete</h4></div><div ng-switch-default><div class=wrapper-title><h3>Login</h3><p class=lead>Please log in to continue</p></div><form ng-submit=login()><div class=form-group-underline><input ng-model=credentials.username type=email name="credentialsEmail"><label for=credentialsEmail tabindex=-1><span>Email Address</span></label></div><div class=form-group-underline><input ng-model=credentials.password type=password name="credentialsPassword"><label for=credentialsPassword tabindex=-1><span>Password</span></label></div><div class=wrapper-xs><button class="btn btn-block btn-primary"><span>Login</span></button> <a class="btn btn-block btn-default" ng-if=!$root.applicationData.disable.signup ui-sref=signup>Create new account</a> <a class="btn btn-block btn-link" ui-sref=login.forgot>Forgotten your password?</a></div></form></div></div></div></div>'), 
    $templateCache.put("routes/myaccount/myaccount.html", '<div><div class="wrapper bg-white"><div class=container><div class="text-wrap text-center text-sm-left"><div class=row><div class="col-xs-6 col-xs-offset-3 col-sm-2 col-sm-offset-0 stacked-xs"><div class=avatar><img preload-image aspect=100 ng-src="{{$root.asset.avatarUrl($root.user.persona, \'persona\', {w:80, h:80, cb:$root.avatarCache.buster})}}"></div></div><div class="col-xs-12 col-sm-10"><h3 class=title>{{$root.user.firstName}} {{$root.user.lastName}}</h3><h5 class=text-muted ng-show=$root.user.email>{{$root.user.email}}</h5><h5 class=text-muted ng-hide=$root.user.email>{{$root.user.username}}</h5><div style="max-width: 400px; display:inline-block"><persona-image-replace-form ng-model=$root.user.persona ng-cache=$root.avatarCache.buster></persona-image-replace-form></div></div></div></div></div></div><div class="btn-tabs border-top" ng-if="!$root.applicationData.disable.comments || !$root.applicationData.disable.notes"><div class="row row-flex"><div class=col-xs-12><a class=btn-tab ng-class="{active:settings.activeTab == \'ownedPurchases\'}" ng-click="settings.activeTab = \'ownedPurchases\'"><span>{{ownedPurchases.length}}</span> <span>{{\'Product\' | reword}}<span ng-show="ownedPurchases.length != 1">s</span></span></a></div><div class=col-xs-12><a class=btn-tab ng-class="{active:settings.activeTab == \'licensedPurchases\'}" ng-click="settings.activeTab = \'licensedPurchases\'"><span>{{licensedPurchases.length}}</span> <span>{{\'License\' | reword}}<span ng-show="licensedPurchases.length != 1">s</span></span></a></div><div class=col-xs-12 ng-if=!$root.applicationData.disable.comments><a class=btn-tab ng-class="{active:settings.activeTab == \'comments\'}" ng-click="settings.activeTab = \'comments\'"><span>{{comments.length}}</span> <span>{{\'Comment\' | reword}}<span ng-show="comments.length != 1">s</span></span></a></div><div class=col-xs-12 ng-if=!$root.applicationData.disable.notes><a class=btn-tab ng-class="{active:settings.activeTab == \'notes\'}" ng-click="settings.activeTab = \'notes\'"><span>{{notes.length}}</span> <span>{{\'Note\' | reword}}<span ng-show="notes.length != 1">s</span></span></a></div></div></div><div ng-switch=settings.activeTab><div ng-switch-when=notes class=my-comment-list><a ui-sref={{getParentSref(note.parent)}} class=my-comment-list-item ng-repeat="note in uniqueNotes track by note._id"><div class=text-wrap><strong>{{note.parent.title}}</strong> <span class="small text-muted">{{note.created | formatDate:\'g:ia -j M Y\'}}</span></div></a></div><div ng-switch-when=comments class=my-comment-list><a ui-sref={{getParentSref(comment.parent)}} class=my-comment-list-item ng-repeat="comment in uniqueComments track by comment._id"><div class=text-wrap><strong>{{comment.parent.title}}</strong> <span class="small text-muted">{{comment.created | formatDate:\'g:ia -j M Y\'}}</span></div></a></div><div ng-switch-when=licensedPurchases class=my-comment-list><div class=my-comment-list-item ng-repeat="purchase in licensedPurchases track by purchase._id"><div class=text-wrap><strong>{{purchase.title}}</strong> licensed to you from {{purchase.grantedBy}}</div></div></div><div ng-switch-default><div class=accordion ng-class="{expanded:expanded.purchase == purchase, \'cancelled\':purchase.status == \'cancelled\'}" ng-repeat="purchase in ownedPurchases track by purchase._id"><div class=accordion-title ng-click="toggleExpanded(\'purchase\', purchase)"><div class=container-fluid><div class=text-wrap><h5 class=title><i class="fa fa-angle-right pull-right" ng-class="{\'fa-rotate-90\':expanded.purchase == purchase}"></i> <span><span ng-if=purchase.amount>${{purchase.product.amount / 100 }}/{{purchase.product.frequency[0]}}</span> {{purchase.product.title}}</span></h5><div style=margin-top:5px class=small><span ng-if=purchase.expires><span ng-if=purchase.renew>Next payment</span><span ng-if=!purchase.renew>Expires</span> <span>{{purchase.expiryDate | formatDate:\'j M Y\'}} <span class=text-muted>({{purchase.expiryDate | timeago}})</span></span></span></div></div></div></div><div class=accordion-body ng-if="expanded.purchase == purchase"><div class=container ng-switch=purchase.status><div class=text-wrap ng-switch-when=cancelled><em class=small>This purchase has been cancelled and will expire {{purchase.expiryDate | formatDate:\'g:ia l j M Y\'}}</em></div><div class=text-wrap ng-switch-default><div ng-if=purchase.owned><div ng-if="purchase.limit > 1"><h6>Licensed to {{licensesUsed(purchase)}} of {{purchase.limit-1}}</h6><p class=help-block>Manage your license invitations below</p><div class="panel panel-default"><div class=table-responsive><table class=table><tbody><tr ng-repeat="license in purchase.managedLicense track by license._id"><td>{{license.firstName}}</td><td>{{license.lastName}}</td><td><em class=text-muted>{{license.username}}</em></td><td></td></tr></tbody></table></div></div><div ng-if=licensesAvailable(purchase)><h6>Invite a new user</h6><p class=help-block>To invite a new user add the details below and press \'invite\'</p><div class="panel panel-default"><div class=panel-body><div class=row><div class="form-group col-xs-12 col-sm-6"><label>First Name</label><input class="form-control small" ng-model=_proposedLicense.firstName placeholder="First Name"></div><div class="form-group col-xs-12 col-sm-6"><label>Last Name</label><input class="form-control small" ng-model=_proposedLicense.lastName placeholder="Last Name"></div><div class="form-group col-xs-12"><label>Email Address</label><input type=email class="form-control small" ng-model=_proposedLicense.email placeholder="Email Address"></div><div class="form-group col-xs-12"><a class="btn btn-primary" ng-disabled=_proposedLicense.processing ng-click=sendInvite(purchase)><span>Invite</span> <i class=fa ng-class="{\'fa-spinner fa-spin\':_proposedLicense.processing, \'fa-paper-plane\':!_proposedLicense.processing}"></i></a></div></div></div></div></div></div><div ng-if=purchase.method ng-controller=PaymentMethodReplaceController><h6>Payment Method</h6><div class="panel panel-default"><div class=panel-body ng-switch=settings.state><div ng-switch-default><p>{{purchase.method.title}}</p><a class="btn btn-primary btn-sm" ng-click="settings.state = \'replace\'"><span>Change / Update</span> <i class="fa fa-pencil-square-o"></i></a></div><div class=text-center ng-switch-when=processing><i class="fa fa-spinner fa-spin fa-2x"></i><div>Processing</div></div><div ng-switch-when=error><h6>Error replacing payment method</h6><pre>{{settings.errorMessage | json}}</pre><a class="btn btn-primary" ng-click="settings.state = \'replace\'"><span>Back</span></a></div><div ng-switch-when=replace><p class=help-block>Update your payment details below</p><div class=form-group><label>Name on Card *</label><input placeholder="Card Number eg. (Kevin Durant)" class=form-control ng-model="settings.addCard.name"></div><div class=form-group><label>Card Number *</label><input placeholder="Card Number eg. (0000-0000-0000-0000)" class=form-control ng-model="settings.addCard.number"></div><div class=row><div class="form-group col-xs-6 col-sm-4"><label>Exp Month *<em class="small text-muted">(MM)</em></label><input placeholder=MM class=form-control ng-model="settings.addCard.exp_month"></div><div class="form-group col-xs-6 col-sm-4"><label>Exp Year *<em class="small text-muted">(YYYY)</em></label><input placeholder=YYYY class=form-control ng-model="settings.addCard.exp_year"></div><div class="form-group col-xs-6 col-sm-4"><label>CVC *</label><input placeholder=CVC class=form-control ng-model="settings.addCard.cvc"></div></div><div ng-if=hasErrors() class="alert alert-warning"><label>Please correct the following errors before continuing</label><div ng-repeat="error in errors"><i class="fa fa-exclamation"></i> {{error}}</div></div><div class=row><div class=col-sm-6><a class="btn btn-primary" ng-disabled=invalid ng-click="replaceWithNewCard(purchase, settings.addCard)"><span>Update Card</span> <i class="fa fa-angle-right"></i></a></div><div class="col-sm-6 text-sm-right"><a class="btn btn-default" ng-click="settings.state = null"><span>Cancel</span></a></div></div></div></div></div></div><div ng-if=purchase.transactions.length><h6>Payment History</h6><div class="panel panel-default"><table class="table table-striped"><thead><tr><td>Transaction ID</td><td>Date</td><td>Amount</td><td>Currency</td></tr></thead><tbody><tr ng-repeat="transaction in purchase.transactions track by transaction._id"><td>{{transaction._id}}</td><td>{{transaction.created | formatDate:\'j M Y\'}}</td><td>{{transaction.amount / 100 | currency}}</td><td class=text-uppercase>{{transaction.currency}}</td></tr></tbody></table></div></div><div ng-if=purchase.expires class=border-top style="padding-top: 20px"><div ng-show="purchase.mode == \'cancel\'"><p class=help-block>Are you sure you want to cancel {{purchase.title}}?</p><a class="btn btn-default btn-sm" ng-click="purchase.mode = null">No</a> <a class="btn btn-danger btn-sm" ng-click=cancelSubscription(purchase)>Yes, Confirm Cancel</a></div><div ng-hide="purchase.mode == \'cancel\'"><a class="btn btn-danger btn-sm" ng-click="purchase.mode = \'cancel\'">Cancel \'{{purchase.title}}\'</a></div></div></div></div></div></div></div></div></div></div>'), 
    $templateCache.put("routes/reset/reset.html", '<div class=wrapper><div class=container ng-switch=settings.state><div class=text-wrap-sm><div class="alert alert-danger" ng-show=settings.errorMessage><i class="fa fa-warning"></i> <span>{{settings.errorMessage}}</span></div><div class=text-center ng-switch-when=processing><i class="fa lnr lnr-sync fa-spin fa-3x"></i><h4>Signing you in</h4></div><div class=text-center ng-switch-when=complete><h4>Log in Complete</h4></div><div ng-switch-when=error><h1 class=h2>Invalid Token</h1><p>This token has already been used or has expired<br>To get a new token click the button below and instructions will be emailed to you.</p><a class="btn btn-primary" ui-sref=login.forgot><span>Send new token</span><i class="fa fa-chevron-right"></i></a></div><div ng-switch-default><h3 class=wrapper-title>{{\'Update your details\' | reword}}</h3><form ng-submit=update()><div class=row><div class="form-group-underline col-sm-6"><input ng-model="persona.firstName"><label><span>First Name</span></label></div><div class="form-group-underline col-sm-6"><input ng-model="persona.lastName"><label><span>Last Name</span></label></div></div><div class=form-group-underline><input ng-model=persona.password type="password"><label><span>Type your new password</span></label></div><div class=form-group-underline><input ng-model=persona.confirmPassword type="password"><label><span>Confirm new password</span></label></div><div class=wrapper-xs><button class="btn btn-primary"><span>Continue</span> <i class="fa fa-angle-right"></i></button></div></form></div></div></div></div>'), 
    $templateCache.put("routes/search/search.html", "<div class=wrapper><div class=container><div class=text-wrap><h1 class=wrapper-title>Search</h1><div></div></div></div></div>"), 
    $templateCache.put("routes/signup/signup.html", '<div class=wrapper><div class=container ng-switch=settings.state><div class=text-wrap><div class="alert alert-danger" ng-show=settings.error><i class="fa fa-warning"></i> <span>{{settings.error}}</span></div><div class=text-center ng-switch-when=processing><i class="lnr lnr-sync fa fa-spin fa-3x"></i><h4>Creating your account</h4></div><div class=text-center ng-switch-when=complete><h4>Signup Complete</h4></div><div ng-switch-default><h3>Signup</h3><p class=lead>Create your new {{$root.applicationData.siteName}} account</p><form ng-submit=signup()><div class=row><div class="form-group-underline col-sm-6"><input ng-model=credentials.firstName name="credentialsFirstName"><label for=credentialsFirstName><span>First Name</span></label></div><div class="form-group-underline col-sm-6"><input ng-model=credentials.lastName name="credentialsLastName"><label for=credentialsLastName><span>Last Name</span></label></div></div><div class=form-group-underline><input ng-model=credentials.username name=credentialsEmail type="email"><label for=credentialsEmail><span>Email Address</span></label></div><div class=form-group-underline><input ng-model=credentials.password name=credentialsPassword type="password"><label for=credentialsPassword><span>Create a password</span></label></div><div class=form-group-underline><input ng-model=credentials.confirmPassword name=credentialsConfirmPassword type="password"><label for=credentialsConfirmPassword><span>Confirm your password</span></label></div><div class=wrapper-xs><div class=row><div class=col-sm-5><button class="btn btn-primary btn-block"><span>Create account</span></button></div><div class="col-sm-5 col-sm-offset-2"><a class="btn btn-block btn-link" ui-sref=login.form>Already have an account?</a></div></div></div></form></div></div></div></div>');
} ]);